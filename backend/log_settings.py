import time

from logging import config, Formatter


class GMTFormatter(Formatter):
    converter = time.gmtime


LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "lines": {
            "format":
                "%(asctime)s [%(levelname)s] %(pathname)s %(lineno)d %(message)s",
            '()':
                GMTFormatter,
        },
        "simple": {
            "format": "%(asctime)s [%(levelname)s] %(message)s",
            '()': GMTFormatter,
        }
    },
    "handlers": {
        "default": {
            "level": "INFO",
            "class": "logging.StreamHandler",
            "formatter": "simple"
        },
        "debug": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "lines"
        }
    },
    "loggers": {
        "": {
            "handlers": ["default"],
            "level": "INFO",
            "propagate": False
        }
    }
}


def config_logging():
    config.dictConfig(LOGGING)
