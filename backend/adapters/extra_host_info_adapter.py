from itertools import groupby
from logging import getLogger
from typing import List


logger = getLogger(__name__)


class ExtraHostInfoAdapter:
    @staticmethod
    def convert(hosts: List[dict], hosts_extra_data: dict, short_reports_info: List[dict]):
        short_host_reports = {}
        for k, v in groupby(short_reports_info, key=lambda r: r["host"]):
            if not short_host_reports.get(k):
                short_host_reports[k] = []
                short_host_reports[k].extend(list(v))
                continue
            short_host_reports[k].extend(list(v))

        for host_data in hosts:
            host_data["extra_data"] = None
            if hosts_extra_data[host_data["host"]]:
                extra = hosts_extra_data[host_data["host"]][0]  # last report sorted by -date

                if not extra["report"]:
                    continue

                host_data["extra_data"] = {
                    "os_fullname": extra["report"]["host_info"]["os_fullname"],
                    "warning[]": len(extra["report"]["warning[]"]),
                    "suggestion[]": len(extra["report"]["suggestion[]"]),
                    "hardening_index": extra["report"]["host_finding"]["hardening_index"]
                }

                host_data["reports"] = short_host_reports.get(host_data["host"], [])
        return hosts


class HostFullInfoAdapter:
    @staticmethod
    def convert(host: dict, extra_host_data: List[dict]):
        host_ip = host["host"]
        if extra_host_data and extra_host_data[host_ip]:
            extra = extra_host_data[host_ip][0]  # last report sorted by -date
            if not extra["report"]:
                return None

            host["extra_data"] = {
                "os_fullname": extra["report"]["host_info"]["os_fullname"],
                "warning[]": extra["report"]["warning[]"],
                "suggestion[]": extra["report"]["suggestion[]"],
                "hardening_index": extra["report"]["host_finding"]["hardening_index"],
            }
            reports = []
            for report_data in extra_host_data[host_ip]:
                if report_data["report"].get("suggestion[]"):
                    report_data["report"].pop("suggestion[]")
                    report_data["report"].pop("warning[]")
                    report_data["report"].pop("host_info")
                    report_data.pop("host")
                    report_data["suggestions_count"] = report_data["report"]["suggestions_count"]
                    report_data["warnings_count"] = report_data["report"]["warnings_count"]
                    report_data["report"]["suggestions_count"] = report_data["suggestions_count"]
                    report_data["report"]["warnings_count"] = report_data["warnings_count"]
                else:
                    logger.warning(f"Unexpected report data: {report_data['report']}")
                    report_data["suggestions_count"] = -1
                    report_data["warnings_count"] = -1
                    report_data["report"]["suggestions_count"] = -1
                    report_data["report"]["warnings_count"] = -1
                reports.append(report_data)

            host["reports"] = reports
            host["extra_data"]["reports"] = reports
        return host

