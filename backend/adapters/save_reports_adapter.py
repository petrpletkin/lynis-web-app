from datetime import datetime
from typing import List, Dict


class SaveReportsAdapter:
    @staticmethod
    def convert(hosts_data: List[Dict], reports: List):
        result = []
        total_data = zip(hosts_data, reports)
        for host_data, report in total_data:
            report = {
                "host": host_data["host"],
                "report": report,
                "created_at": datetime.utcnow()
            }
            result.append(report)
        return result
