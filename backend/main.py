from logging import WARNING

import asyncssh
from fastapi import FastAPI

from report_scheduler.scheduler import scheduler_instance
from routers import hosts, reports, user, doc_api
from storage.mongo.create_connection import get_or_create_mongo_client
from repositories.user_repository import UserRepository

asyncssh.logging.set_log_level(WARNING)

tags_metadata = [
    {
        "name": "hosts",
        "description": "Operation with hosts metadata",
    },
    {
        "name": "reports",
        "description": "Operation with reports metadata",
    }
]

app = FastAPI(
    title="Lynis Collector API",
    description="API for operate with reports and host data.",
    version="0.0.0",
    openapi_tags=tags_metadata
)


@app.on_event("startup")
async def startup():
    get_or_create_mongo_client()
    await UserRepository.check_default_user()
    scheduler_instance.start()


api_prefix = "/api"

app.include_router(hosts.router, tags=["hosts"], prefix=api_prefix)
app.include_router(reports.router, tags=["reports"], prefix=api_prefix)
app.include_router(user.router, tags=["user"], prefix=api_prefix)
app.include_router(doc_api.router)
