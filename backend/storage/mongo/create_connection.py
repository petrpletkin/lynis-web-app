import os

import motor.motor_asyncio as aiomotor

MONGO_CLIENT = None


def get_or_create_mongo_client():
    global MONGO_CLIENT
    if not MONGO_CLIENT:
        MONGO_CLIENT = aiomotor.AsyncIOMotorClient(os.environ["MONGODB_URL"])

    return MONGO_CLIENT
