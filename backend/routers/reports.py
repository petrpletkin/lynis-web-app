from typing import List

from fastapi import APIRouter
from fastapi.background import BackgroundTasks
from repositories.report_repository import ReportRepository
from repositories.user_repository import UserRepository
from report_scheduler.scheduler import ReportScheduler
from .request_models.reports_models import *
from .request_models.hosts_models import NewHostModel
from services.auth_user_service import TokenData, Depends, AuthService

router = APIRouter(prefix="/reports")


@router.get("", response_model=List[ReportModel])
async def get_reports_preview(
        user: TokenData = Depends(AuthService.get_current_user)
):
    user_data = await UserRepository.get_user_by_username(user.username)
    reports = await ReportRepository.get_reports_preview(user_data["hosts"])
    return reports


@router.put("", response_model=List[NewHostModel])
async def create_report(
        hosts: List[NewHostModel], background: BackgroundTasks,
        user: TokenData = Depends(AuthService.get_current_user)
):
    hosts = [host.dict() for host in hosts]
    background.add_task(ReportScheduler.create_reports, hosts)
    return hosts


@router.get("/{report_id}", response_model=ReportExtraModel)
async def get_report_by_id(
        report_id: str,
        user: TokenData = Depends(AuthService.get_current_user)
):
    report = await ReportRepository.get_by_id(report_id)
    return report
