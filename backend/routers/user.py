from logging import getLogger

from fastapi import APIRouter, Depends

from repositories.user_repository import UserRepository
from services.auth_user_service import AuthService
from .request_models.user_models import *

logger = getLogger(__name__)


router = APIRouter(prefix="/users")


@router.post("/auth", response_model=Token)
async def authenticate(body: AuthBody):
    user_data = await AuthService.auth_user(body.username, body.password)
    token = AuthService.create_token_for_user(user_data)
    return {"access_token": token, "token_type": "bearer"}


@router.get("/info", response_model=UserBody)
async def get_user_info(user_data: TokenData = Depends(AuthService.get_current_user)):
    user = await UserRepository.get_user_by_username(user_data.username)
    return user
