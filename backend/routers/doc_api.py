from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm

from services.auth_user_service import AuthService
from .request_models.user_models import Token

router = APIRouter()


@router.post("/token", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user_data = await AuthService.auth_user(form_data.username, form_data.password)
    token = AuthService.create_token_for_user(user_data)
    return {"access_token": token, "token_type": "bearer"}
