from datetime import datetime
from typing import Dict, Optional

from .base_mongo_model import BaseMongoModel, BaseModel


class ReportModel(BaseMongoModel):
    host: str
    created_at: datetime


class ReportExtraModel(ReportModel):
    report: Optional[Dict] = {}