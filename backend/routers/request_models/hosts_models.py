from datetime import datetime
from typing import Optional, List

from pydantic import BaseModel

from .base_mongo_model import BaseMongoModel


class NewHostModel(BaseModel):
    host: str
    username: str
    port: int
    alias: Optional[str] = ""


class HostModel(BaseMongoModel):
    host: str
    username: str
    port: int
    alias: Optional[str] = ""
    created_at: datetime
    last_update_timestamp: Optional[datetime] = None
    is_running_now: Optional[bool] = False
    extra_data: Optional[dict] = None
    reports: Optional[List[dict]] = None
