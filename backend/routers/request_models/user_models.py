from typing import Optional, List

from .base_mongo_model import BaseModel, BaseMongoModel


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None
    email: Optional[str] = None


class AuthBody(BaseModel):
    username: str
    password: str


class UserBody(BaseMongoModel):
    username: str
    email: str
    hosts: Optional[List[str]] = []
