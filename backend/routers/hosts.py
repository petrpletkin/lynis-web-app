from logging import getLogger

from fastapi import APIRouter, BackgroundTasks

from adapters.extra_host_info_adapter import ExtraHostInfoAdapter, HostFullInfoAdapter
from report_scheduler.scheduler import ReportScheduler
from repositories.host_repository import HostRepository
from repositories.report_repository import ReportRepository
from repositories.user_repository import UserRepository
from services.auth_user_service import TokenData, Depends, AuthService
from .request_models.hosts_models import *
from .request_models.reports_models import ReportModel

logger = getLogger(__name__)

router = APIRouter(prefix="/hosts")


@router.get("", response_model=List[HostModel])
async def get_hosts_preview(user: TokenData = Depends(AuthService.get_current_user)):
    user_data = await UserRepository.get_user_by_username(user.username)
    hosts_data = await HostRepository.get_hosts_required_data(user_data["hosts"])

    hosts_ip = [host_data["host"] for host_data in hosts_data]
    info_from_reports = await ReportRepository.get_hosts_info(hosts_ip, length=1)
    short_reports_info = await ReportRepository.get_reports_preview_by_host(hosts_ip)
    hosts_extra_data = ExtraHostInfoAdapter.convert(hosts_data, info_from_reports, short_reports_info)

    return hosts_extra_data


@router.get("/{host_id}", response_model=HostModel)
async def get_host_info(
        host_id: str, user: TokenData = Depends(AuthService.get_current_user)
):
    host_data = await HostRepository.get_by_id(host_id)

    if host_data:
        info_from_reports = await ReportRepository.get_hosts_info([host_data["host"]])
        hosts_extra_data = HostFullInfoAdapter.convert(host_data, info_from_reports)
        host_data = hosts_extra_data

    return host_data


@router.put("/{host_id}", response_model=NewHostModel)
async def update_host_info(
        host_id: str,
        host_data: NewHostModel,
        user: TokenData = Depends(AuthService.get_current_user)
):
    await HostRepository.update_host_by_id(host_id, host_data.dict())
    return host_data


@router.delete("/{host_id}", response_model=HostModel)
async def remove_host(
        host_id: str, user: TokenData = Depends(AuthService.get_current_user)
):
    host_data = await HostRepository.get_by_id(host_id)
    await HostRepository.delete_host(host_id)
    await ReportRepository.delete_by_host(host_data["host"])
    await UserRepository.delete_host(user.username, host_data["host"])
    return host_data


@router.post("", response_model=HostModel)
async def add_new_host(
        host_data: NewHostModel,
        background: BackgroundTasks,
        user: TokenData = Depends(AuthService.get_current_user),
):
    await UserRepository.add_new_host(user.username, host_data.host)
    host_instance = await HostRepository.create_new_host(host_data.dict())
    background.add_task(ReportScheduler.create_reports, [host_instance])
    return HostModel(**host_instance)


@router.get("/{host_id}/reports", response_model=List[ReportModel])
async def get_reports_by_host(
        host_id: str,
        user: TokenData = Depends(AuthService.get_current_user)
):
    host_data = await HostRepository.get_by_id(host_id)
    reports = await ReportRepository.get_reports_preview_by_host(
        host_data["host"]
    )
    return reports
