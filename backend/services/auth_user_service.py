from datetime import datetime as dt, timedelta as delta
from logging import getLogger

import jwt
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer

from repositories.user_repository import UserRepository
from routers.request_models.user_models import TokenData

logger = getLogger(__name__)


ALGORITHM_FOR_JWT = "HS256"
SECRET_KEY_FOR_JWT = "sUperSecretkEy"
TOKEN_EXPIRES_DAYS = 2

credentials_exc = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Could not validate credentials",
    headers={"WWW-Authenticate": "Bearer"},
)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


class AuthService:
    @staticmethod
    async def auth_user(username: str, password: str):
        user = await UserRepository.get_user_by_username(username)
        if not user:
            raise HTTPException(403, detail="User not found.")
        if not UserRepository.is_password_valid(password, user["password"]):
            raise HTTPException(403, detail=f"Invalid password for: {username}")
        return user

    @staticmethod
    def create_token_for_user(user_data: dict):
        data = {
            "username": user_data["username"],
            "email": user_data["email"],
            "exp": dt.utcnow() + delta(days=TOKEN_EXPIRES_DAYS)
        }
        encoded_data = jwt.encode(
            data, SECRET_KEY_FOR_JWT, algorithm=ALGORITHM_FOR_JWT
        )
        return encoded_data

    @staticmethod
    async def get_current_user(token: str = Depends(oauth2_scheme)):
        try:
            payload = jwt.decode(
                token, SECRET_KEY_FOR_JWT, algorithms=[ALGORITHM_FOR_JWT]
            )
            expires_date = payload.pop("exp")

            if expires_date <= dt.utcnow().second:
                raise credentials_exc
            token_data = TokenData(**payload)
        except Exception as exc:
            logger.error(f"Got exec when decode user data: {exc}")
            raise credentials_exc

        return token_data
