import uvicorn

from log_settings import config_logging


def start():
    config_logging()
    uvicorn.run("main:app", host="0.0.0.0", port=8888, log_level="info")


if __name__ == '__main__':
    start()
