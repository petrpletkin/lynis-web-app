from storage.mongo.create_connection import get_or_create_mongo_client
from datetime import datetime
from typing import List, Dict, Union
from bson import ObjectId
import asyncio


class ReportRepository:
    @classmethod
    async def get_by_id(cls, report_id: str):
        client = get_or_create_mongo_client()
        report = await client.collector.reports.find_one(
            {"_id": ObjectId(report_id)}
        )
        return report

    @classmethod
    async def get_reports_preview(cls, hosts: List[str]):
        client = get_or_create_mongo_client()
        reports_cursor = client.collector.reports.find(
            {
                "host": {"$in": hosts}
            },
            {"host": 1, "created_at": 1, "_id": 1}
        )
        return await reports_cursor.to_list(length=None)

    @classmethod
    async def get_all_reports(cls):
        client = get_or_create_mongo_client()
        reports_cursor = client.collector.reports.find({})
        return await reports_cursor.to_list(length=None)

    @classmethod
    async def save_reports(cls, reports_for_save: List[Dict]):
        client = get_or_create_mongo_client()
        bulk_tasks = []
        for report in reports_for_save:
            task = asyncio.ensure_future(
                client.collector.reports.update_one(
                    {"host": report["host"]},
                    {
                        "$set": {
                            "report": report["report"],
                            "created_at": report["created_at"]
                        }
                    },
                    upsert=True)
            )
            bulk_tasks.append(task)
        await asyncio.gather(*bulk_tasks)

    @classmethod
    async def save_reports(cls, reports_for_save: List[Dict]):
        client = get_or_create_mongo_client()
        await client.collector.reports.insert_many(reports_for_save)

    @classmethod
    async def get_hosts_info(cls, hosts_ip: List[str], length: int = None):
        client = get_or_create_mongo_client()
        result = {}
        for host in hosts_ip:
            cursor = client.collector.reports.find(
                {"host": host},
                {
                    "host": 1,
                    "created_at": 1,
                    "report.host_info.os_fullname": 1,
                    "report.warning[]": 1,
                    "report.suggestion[]": 1,
                    "report.host_finding.hardening_index": 1,
                    "report.suggestions_count": 1,
                    "report.warnings_count": 1
                }, sort=[("created_at", -1)]
            )
            reports = await cursor.to_list(length=length)
            result[host] = reports
        return result

    @classmethod
    async def get_reports_preview_by_host(cls, host_data: Union[str, List[str]] ):
        hosts = host_data if isinstance(host_data, list) else list(host_data)
        client = get_or_create_mongo_client()
        reports_cursor = client.collector.reports.find(
            {"host": {"$in": hosts}},
            {
                "host": 1,
                "created_at": 1,
                "_id": 1,
                "report.suggestions_count": 1,
                "report.warnings_count": 1
            }
        ).sort("created_at", -1)
        return await reports_cursor.to_list(length=None)

    @classmethod
    async def delete_by_host(cls, host: str):
        client = get_or_create_mongo_client()
        await client.collector.reports.delete_many({"host": host})

    @classmethod
    async def delete_reports_by_ids(cls, report_ids: List[str]):
        client = get_or_create_mongo_client()
        await client.collector.reports.delete_many({"_id": {"$in": report_ids}})

    @classmethod
    async def get_last_report(cls, host: str):
        client = get_or_create_mongo_client()
        return await client.collector.reports.find_one({"host": host}, sort=[("created_at", -1)])

