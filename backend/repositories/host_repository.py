from datetime import datetime
from typing import List

from bson import ObjectId

from storage.mongo.create_connection import get_or_create_mongo_client


class HostRepository:
    @classmethod
    async def get_by_id(cls, host_id: str):
        client = get_or_create_mongo_client()
        return await client.collector.hosts.find_one(
            {"_id": ObjectId(host_id)}
        )

    @classmethod
    async def get_by_host(cls, host: str):
        client = get_or_create_mongo_client()
        return await client.collector.hosts.find_one(
            {"host": host}
        )

    @classmethod
    async def get_hosts_required_data(cls, hosts: List[str]):
        client = get_or_create_mongo_client()
        cursor = client.collector.hosts.find({"host": {"$in": hosts}})
        return await cursor.to_list(None)

    @classmethod
    async def create_new_host(cls, host_data: dict):
        exist_host_data = await cls.get_by_host(host_data["host"])

        if not exist_host_data:
            host_data["created_at"] = datetime.utcnow()
            host_data["last_update_timestamp"] = None
            host_data["is_running_now"] = False
            client = get_or_create_mongo_client()
            result = await client.collector.hosts.insert_one(host_data)
            return await client.collector.hosts.find_one(
                {"_id": result.inserted_id}
            )

        return exist_host_data

    @classmethod
    async def get_all_hosts(cls):
        client = get_or_create_mongo_client()
        cursor = client.collector.hosts.find({}).sort("created_at", -1)
        return await cursor.to_list(length=None)

    @classmethod
    async def update_last_timestamp(cls, timestamp: datetime, hosts: List[str]):
        host_ips = [data["host"] for data in hosts]
        client = get_or_create_mongo_client()
        await client.collector.hosts.update_many(
                {
                    "host": {
                        "$in": host_ips
                    }
                },
                {
                    "$set": {
                        "last_update_timestamp": timestamp
                    }
                }
        )

    @classmethod
    async def set_status_running(cls, hosts: List[dict], status: bool):
        host_ips = [data["host"] for data in hosts]
        client = get_or_create_mongo_client()
        await client.collector.hosts.update_many(
            {"host": {
                "$in": host_ips
            }},
            {
                "$set": {"is_running_now": status}
            },
            upsert=True
        )

    @classmethod
    async def delete_host(cls, host_id: str):
        client = get_or_create_mongo_client()
        return await client.collector.hosts.delete_one(
            {"_id": ObjectId(host_id)}
        )

    @classmethod
    async def update_host_by_id(cls, host_id: str, new_host_data: dict):
        client = get_or_create_mongo_client()
        await client.collector.hosts.update_one(
            {"_id": ObjectId(host_id)},
            {
                "$set": new_host_data
            }
        )
