from hashlib import md5
import os
from motor import motor_asyncio as aiomotor
from storage.mongo.create_connection import get_or_create_mongo_client


class UserRepository:
    @staticmethod
    async def get_user_by_username(username: str):
        client = get_or_create_mongo_client()
        user = await client.collector.users.find_one(
            {
                "username": username,
             }
        )
        return user

    @staticmethod
    def is_password_valid(password: str, hashed_password: str):
        if md5(password.encode()).hexdigest() == hashed_password:
            return True

    @staticmethod
    async def _user_already_has_host(
            username: str, host: str, client: aiomotor.AsyncIOMotorClient
    ):
        return await client.collector.users.find_one(
            {"username": username, "hosts": host}
        )

    @classmethod
    async def add_new_host(cls, username: str, host: str):
        client = get_or_create_mongo_client()
        already_has_host_flag = await cls._user_already_has_host(username, host, client)
        if not already_has_host_flag:
            client.collector.users.update_one(
                {"username": username},
                {"$push": {"hosts": host}},
                upsert=True
            )

    @classmethod
    async def delete_host(cls, username: str, host: str):
        client = get_or_create_mongo_client()
        await client.collector.users.update_one(
            {"username": username},
            {
                "$pull": {
                    "hosts": host
                }
            }
        )
    @staticmethod
    async def _create_new_user(username: str, password: str, email: str = None):
        client = get_or_create_mongo_client()
        user = {
            "username": username,
            "password": md5(password.encode()).hexdigest(),
            "email": email or f"{username}@{username}.com",
            "hosts": []
        }
        await client.collector.users.insert_one(user)

    @classmethod
    async def check_default_user(cls):
        username = os.environ["DEFAULT_ADMIN_USERNAME"]
        password = os.environ["DEFAULT_ADMIN_PASSWORD"]
        existing_user = await cls.get_user_by_username(username)
        if not existing_user:
            await cls._create_new_user(username, password)
