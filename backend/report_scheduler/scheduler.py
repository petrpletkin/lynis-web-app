import datetime
import os
from asyncio import gather, ensure_future
from logging import getLogger
from subprocess import call
from time import monotonic
from typing import List

import asyncssh
import ujson
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from apscheduler.triggers.cron import CronTrigger
from apscheduler.triggers.interval import IntervalTrigger

from adapters.save_reports_adapter import SaveReportsAdapter
from repositories.host_repository import HostRepository
from repositories.report_repository import ReportRepository

logger = getLogger(__name__)

ssh_user = "lynis-reporter"
create_report_cmd = "sudo lynis audit system --logfile ~/lynis.log --report-file ~/lynis-report.dat -q"
lynis_report_filename = "lynis-report.dat"
lynis_log_filename = "lynis.log"
json_report_filename = "lynis-report.json"
remote_report_file_path = "~/" + lynis_report_filename
remote_log_file_path = "~/" + lynis_log_filename

local_reports_folder = os.path.join(os.getcwd(), "reports")
converter_script_path = os.path.join(os.getcwd(), "converter/lynis-report-converter.pl")

MAX_REPORT_COUNT = os.environ["MAX_REPORT_HISTORY"]


class ReportScheduler:

    default_private_keys = [
        asyncssh.read_private_key("~/.ssh/id_rsa",
                                  os.environ["PRIVATE_KEY_PHRASE"])
    ]

    @classmethod
    async def create_reports(cls, hosts: List[dict] = None):
        hosts = await HostRepository.get_all_hosts() if not hosts else hosts
        await HostRepository.set_status_running(hosts, True)
        start_time = monotonic()
        if hosts:
            await cls.batch_create_lynis_reports(hosts)
            logger.info(f"For reports: {monotonic() - start_time}")

            scp_time = monotonic()
            await cls.batch_copy_lynis_reports(hosts)
            logger.info(f"For scp: {monotonic() - scp_time}")

            convert_time = monotonic()
            cls.batch_convert_report_to_json(hosts)
            logger.info(f"For convert: {monotonic() - convert_time}")

            logger.info(f"Time spend for collect: {monotonic() - start_time}")
            await cls._save_reports(hosts)
            await cls._set_last_updated_timestamp(hosts)
            await cls.clean_old_reports(hosts)
        await HostRepository.set_status_running(hosts, False)

    @classmethod
    async def _save_reports(cls, hosts: List[dict]):
        reports_data = []
        for host_data in hosts:
            json_report_path = cls._get_json_report_path(host_data)
            if os.path.exists(json_report_path):
                with open(json_report_path, "r") as r_stream:
                    json_data = ujson.load(r_stream)
                try:
                    logger.info(f"Converting {host_data['host']} into correct view")
                    converted_data = cls._convert_report_data_to_view(json_data)
                    reports_data.append(converted_data)
                    os.remove(json_report_path)
                except Exception as unexpected:
                    logger.error(f"Got error trying to convert to correct view: {unexpected}")
                    error_report = {
                        "error_message": "Report not created. Check logs and retry again!"
                    }
                    reports_data.append(error_report)
            else:
                error_report = {
                    "error_message": "Report not created. Check logs and retry again!"
                }
                reports_data.append(error_report)
        reports_for_save = SaveReportsAdapter.convert(hosts, reports_data)
        await ReportRepository.save_reports(reports_for_save)

    @classmethod
    async def _set_last_updated_timestamp(cls, hosts: List[str]):
        timestamp = datetime.datetime.utcnow()
        await HostRepository.update_last_timestamp(timestamp, hosts)

    @staticmethod
    def _get_json_report_path(host_data):
        server_ip = host_data["host"]
        json_report_path = os.path.join(
            local_reports_folder, server_ip, json_report_filename
        )
        return json_report_path

    @classmethod
    async def create_lynis_report(cls, host_data: dict):
        server_ip = host_data["host"]
        async with asyncssh.connect(
                server_ip,
                port=host_data["port"],
                username=host_data["username"],
                client_keys=cls.default_private_keys,
                known_hosts=None
        ) as ssh:
            logger.info(f"Run report on {server_ip} via SSH conn")
            result = await ssh.run(create_report_cmd, check=True)
            if result.exit_status != 0:
                logger.info('Task %s exited with status %s:' %
                            (server_ip, result.exit_status))
                logger.info(result.stderr, end='')
            else:
                logger.info('Task %s succeeded:' % server_ip)

    @classmethod
    async def batch_create_lynis_reports(cls, hosts_data: List[dict]):
        reporter_tasks = [cls.create_lynis_report(host) for host in hosts_data]
        responses = await gather(*reporter_tasks, return_exceptions=True)
        return responses

    @classmethod
    async def copy_lynis_reports(cls, host_data: dict):
        server_ip = host_data["host"]
        dir_path = os.path.join(local_reports_folder, server_ip)
        os.mkdir(dir_path) if not os.path.exists(dir_path) else None
        async with asyncssh.connect(
                server_ip,
                port=host_data["port"],
                username=host_data["username"],
                client_keys=cls.default_private_keys,
                known_hosts=None
        ) as ssh:

            logger.info(f"Run collect reports from {server_ip} via SSH conn")
            local_path = os.path.join(local_reports_folder, server_ip, lynis_report_filename)
            await asyncssh.scp(
                (ssh, remote_report_file_path), local_path
            )
            local_path = os.path.join(local_reports_folder, server_ip, lynis_log_filename)
            await asyncssh.scp(
                (ssh, remote_log_file_path), local_path
            )
        logger.info(f"Files from {server_ip} copied")

    @classmethod
    async def batch_copy_lynis_reports(cls, hosts_data: List[dict]):

        collector_tasks = []
        for host in hosts_data:
            task = ensure_future(cls.copy_lynis_reports(host))
            collector_tasks.append(task)
        responses = await gather(*collector_tasks, return_exceptions=True)
        return responses

    @staticmethod
    def err_handler(exc=None):
        logger.error(f"|copy_lynis_reports| Error: {str(exc)}")

    @classmethod
    def create_json_report(cls, host_data: str):
        server_ip = host_data["host"]
        report_path = os.path.join(
            local_reports_folder, server_ip, lynis_report_filename
        )
        json_report_path = cls._get_json_report_path(host_data)
        exit_code = call(
            [f"{converter_script_path} --json -i {report_path} -o {json_report_path}"]
            , shell=True
        )

        if not exit_code == 0:
            logger.error(
                f"|create_json_report| "
                f"Not correct exit_code of script for host: {server_ip}."
                f" See logs!"
            )
        return exit_code

    @classmethod
    def batch_convert_report_to_json(cls, hosts: List[str]):
        for host in hosts:
            cls.create_json_report(host)
        return

    @classmethod
    async def clean_old_reports(cls, hosts: List[dict]):
        for host_data in hosts:
            host_ip = host_data["host"]
            last_reports = await ReportRepository.get_reports_preview_by_host(host_ip)
            if last_reports and len(last_reports) > MAX_REPORT_COUNT:
                reports_for_delete = last_reports[MAX_REPORT_COUNT::]
                reports_ids = [item["_id"] for item in reports_for_delete]
                await ReportRepository.delete_reports_by_ids(reports_ids)

    @classmethod
    def _convert_report_data_to_view(cls, json_data: dict):
        converted_report = {
            "host_finding": {
                "auditor": json_data.get("auditor", ""),
                "hardening_index": json_data.get("hardening_index", 0)
            },
            "warning[]": json_data.get("warning[]", []),
            "warnings_count": len(json_data.get("warning[]", [])),
            "suggestion[]": json_data.get("suggestion[]", []),
            "suggestions_count": len(json_data["suggestion[]"]),
            "manual[]": json_data.get("manual[]", []),
            "vulnerable_packag[]": json_data.get("vulnerable_package[]", []),
            "lynis_info": {
                "lynis_version": json_data.get("lynis_version", ""),
                "lynis_tests_done": json_data.get("lynis_tests_done", ""),
                "lynis_update_available": json_data.get("lynis_update_available", ""),
                "license_key": json_data.get("license_key", ""),
                "report_datetime_start": json_data.get("report_datetime_start", ""),
                "report_datetime_end": json_data.get("report_datetime_end", ""),
                "report_version_major": json_data.get("report_version_major", ""),
                "report_version_minor": json_data.get("report_version_minor", ""),
                "plugin_enabled_phase[]": json_data.get("plugin_enabled_phase1[]", [])
            },
            "host_info": {
                "hostname": json_data.get("hostname", ""),
                "domainname": json_data.get("domainname", ""),
                "resolv_conf_search_domain[]": json_data.get("resolv_conf_search_domain[]", ""),
                "os_fullname": json_data.get("os_fullname", ""),
                "os_kernel_version_full": json_data.get("os_kernel_version_full", ""),
                "package_audit_tool": json_data.get("package_audit_tool", ""),
                "package_audit_tool_found": json_data.get("package_audit_tool_found", ""),
                "memory_size": json_data.get("memory_size", "") + json_data.get("memory_units", ""),
                "selinux": {
                    "status": json_data.get("selinux_status", ""),
                    "mode": json_data.get("selinux_mode", "")
                },
                "cpu_pae": json_data.get("cpu_pae", ""),
                "cpu_nx": json_data.get("cpu_nx", ""),
                "uptime_in_days": json_data.get("uptime_in_days", ""),
                "locate_db": json_data.get("locate_db", ""),
                "available_shell[]": json_data.get("available_shell[]", []),
                "uptime_in_seconds": json_data.get("uptime_in_seconds", ""),
                "is_notebook": json_data.get("notebook", False),
                "is_container": json_data.get("container", False),
                "binary_paths": json_data.get("binary_paths", ",").split(","),
                "cron_job[]": json_data.get("cronjob[]", []),
                "logging_info": {
                    "log_rotation_config_found": json_data.get("log_rotation_config_found", ""),
                    "log_rotation_tool": json_data.get("log_rotation_tool", ""),
                    "syslog_daemon[]": json_data.get("syslog_daemon[]", []),
                    "log_directory[]": json_data.get("log_directory[]", []),
                },
            },
            "network_info": {
                "ipv6_mode": json_data.get("ipv6_mode", ""),
                "ipv6_only": json_data.get("ipv6_only", ""),
                "network_interface[]": json_data.get("network_interface[]", []),
                "network_ipv4_address[]": json_data.get("network_ipv4_address[]", []),
                "network_ipv6_address[]": json_data.get("network_ipv6_address[]", []),
                "default_gateway": json_data.get("default_gateway[]", []),
                "network_mac_address[]": json_data.get("network_mac_address[]", []),
                "name_cache_used": json_data.get("name_cache_used", ""),
                "nameservers": json_data.get("nameserver[]", []),
                "resolv_conf_search_domain": json_data.get("resolv_conf_search_domain[]", []),
                "network_listen_port[]": json_data.get("network_listen_port[]", [])
            },
            "security_info": {
                "firewall_installed": json_data.get("firewall_installed", ""),
                "firewall_software[]": json_data.get("firewall_software[]", ""),
                "firewall_empty_ruleset": json_data.get("firewall_empty_ruleset", ""),
                "package_audit_tool": json_data.get("package_audit_tool", ""),
                "package_audit_tool_found": json_data.get("package_audit_tool_found", ""),
                "package_manager[]": json_data.get("package_manager[]", []),
                "authentication_two_factor_enabled": json_data.get("authentication_two_factor_enabled", ""),
                "authentication_two_factor_required": json_data.get("authentication_two_factor_enabled", ""),
                "ldap_auth_enabled": json_data.get("ldap_auth_enabled", ""),
                "ldap_pam_enabled": json_data.get("ldap_pam_enabled", ""),
                "password_max_days": json_data.get("password_max_days", 0),
                "password_min_days": json_data.get("password_min_days", 0),
                "automation_tool_present": json_data.get("automation_tool_present", ""),
                "automation_tool_running[]": json_data.get("automation_tool_running[]"),
                "pam_pwquality": json_data.get("pam_pwquality", ""),
                "compiler[]": json_data.get("compiler[]", []),
                "compiler_installed": json_data.get("compiler_installed", ""),
                "ids_ips_tooling[]": json_data.get("ids_ips_tooling[]", []),
                "auth_failed_logins_logged": json_data.get("auth_failed_logins_logged", ""),
                "fail2ban_config": json_data.get("fail2ban_config", ""),
                "fail2ban_enabled_service[]": json_data.get("fail2ban_enabled_service[]", []),
                "apparmor_enabled": json_data.get("apparmor_enabled", ""),
                "apparmor_policy_loaded": json_data.get("apparmor_policy_loaded", ""),
                "auth_group_ids_unique": json_data.get("auth_group_ids_unique", ""),
                "auth_group_names_unique": json_data.get("auth_group_names_unique", ""),
                "real_user[]": [data.split(",") for data in json_data.get("real_user[]", [])],
                "home_directory[]": json_data.get("home_directory[]", [])
            },
            "boot_info": {
                "boot_service_tool": json_data.get("boot_service_tool", ""),
                "boot_uefi_booted": json_data.get("boot_uefi_booted", ""),
                "boot_uefi_booted_secure": json_data.get("boot_uefi_booted_secure", ""),
                "linux_default_runlevel": json_data.get("linux_default_runlevel", ""),
                "services_started_at_boot[]": json_data.get("boot_service[]", []),
                "boot_loader": json_data.get("boot_loader", "")
            },
            #  TODO: service info, daemon info

        }
        return converted_report


scheduler_instance = AsyncIOScheduler()
scheduler_instance.add_job(
    ReportScheduler.create_reports,
    CronTrigger.from_crontab(os.environ["REPORT_CRON_STRING"])
)