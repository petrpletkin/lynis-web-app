apt update							# versions prior to Ubuntu 16.04 LTS should use 'apt-get'
apt -y install htmldoc libxml-writer-perl libarchive-zip-perl libjson-perl

pushd /tmp/
wget http://search.cpan.org/CPAN/authors/id/M/MF/MFRANKL/HTML-HTMLDoc-0.10.tar.gz
tar xvf HTML-HTMLDoc-0.10.tar.gz
pushd HTML-HTMLDoc-0.10
perl Makefile.PL
make && make install
popd
wget http://search.cpan.org/CPAN/authors/id/J/JM/JMCNAMARA/Excel-Writer-XLSX-0.95.tar.gz
tar xvf Excel-Writer-XLSX-0.95.tar.gz
pushd Excel-Writer-XLSX-0.95
perl Makefile.PL
make && make install
popd
popd
