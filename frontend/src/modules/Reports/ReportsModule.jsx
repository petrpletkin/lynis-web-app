import { Route } from 'react-router-dom';

import { Reports, Report } from 'features/reports';

export const ReportsModule = {
  getRoutes: () => [
    <Route key="reports" exact path="/reports" component={Reports} />,
    <Route key="report" exact path="/reports/:id" component={Report} />,
  ],
};
