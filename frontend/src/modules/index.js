import { HostsModule } from './Hosts/HostsModule';
import { ReportsModule } from './Reports/ReportsModule';

export const modules = [HostsModule, ReportsModule];
