import React from 'react';
import { useSelector, shallowEqual } from 'react-redux';
import PropTypes from 'prop-types';

import { theme } from 'shared/style/theme';

import { useToken } from 'shared/hooks/useToken';

import {
  AlertContext,
  useAlertInitialValue,
} from 'shared/context/AlertContext';

import {
  Skeleton,
  Box,
  ColorModeScript,
  ChakraProvider,
} from '@chakra-ui/react';

import SideMenu from 'shared/components/SideMenu';
import Alert from 'shared/components/Alert';

import { Auth } from 'features/user';

const App = ({ children }) => {
  const token = useToken();

  const appActionProcessing = useSelector(
    (state) => state.user.appActionProcessing,
    shallowEqual
  );

  const alertValue = useAlertInitialValue();
  return (
    <AlertContext.Provider value={alertValue}>
      <ChakraProvider theme={theme}>
        <ColorModeScript initialColorMode={theme.config.initialColorMode} />
        <Alert />
        <Box h="100%" w="100%">
          <Skeleton
            startColor="teal.500"
            endColor="gray.500"
            isLoaded={!appActionProcessing}
            h="100%"
            w="100%"
          >
            {!token ? (
              <Auth />
            ) : (
              <Box display="flex" h="100%" w="100%">
                <SideMenu />
                <Box h="100%" w="100%" overflow="auto">
                  {children}
                </Box>
              </Box>
            )}
          </Skeleton>
        </Box>
      </ChakraProvider>
    </AlertContext.Provider>
  );
};

App.propTypes = {
  children: PropTypes.element.isRequired,
};

export default React.memo(App);
