import { Route } from 'react-router-dom';

import { Hosts, Host } from 'features/hosts';

export const HostsModule = {
  getRoutes: () => [
    <Route key="hosts" exact path="/hosts" component={Hosts} />,
    <Route key="host" path="/hosts/:id" component={Host} />,
  ],
};
