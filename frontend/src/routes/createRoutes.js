import React from 'react';
import { Redirect, Switch } from 'react-router-dom';

export const createRoutes = (modules) => {
  const routesFromModules = modules.map((module) => module.getRoutes());
  return (
    <Switch>
      <Redirect exact from="/" to="/hosts" />
      {routesFromModules}
    </Switch>
  );
};
