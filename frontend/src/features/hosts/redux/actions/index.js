import { notify, status } from 'features/notification';

export const actionTypes = {
  ACTION_PROCESSING: 'hosts/ACTION_PROCESSING',
  ACTION_FAILURE: 'hosts/ACTION_FAILURE',
  GET_HOSTS: 'hosts/GET_HOSTS',
  GET_HOST: 'hosts/GET_HOST',
  CLEAR_HOST: 'hosts/CLEAR_HOST',
  DELETE_HOST: 'hosts/DELETE_HOST',
  CREATE_ACTION_PROCESSING: 'hosts/CREATE_ACTION_PROCESSING',
  UPDATE_ACTION_PROCESSING: 'hosts/UPDATE_ACTION_PROCESSING',
};

const actionProcessing = (isProcessing = true) => ({
  type: actionTypes.ACTION_PROCESSING,
  payload: isProcessing,
});

const actionFailure = () => ({ type: actionTypes.ACTION_FAILURE });

export const getHosts =
  () =>
  async (dispatch, getState, { api }) => {
    dispatch(actionProcessing());
    const response = await api.hosts.getHosts();
    if (response.success) {
      dispatch({ type: actionTypes.GET_HOSTS, payload: response.data });
    } else dispatch(actionFailure());
  };

export const getHost =
  (id) =>
  async (dispatch, getState, { api }) => {
    dispatch(actionProcessing());
    const response = await api.hosts.getHost(id);
    if (response.success) {
      dispatch({ type: actionTypes.GET_HOST, payload: response.data });
    } else dispatch(actionFailure());
  };

export const clearHost = () => ({ type: actionTypes.CLEAR_HOST });

const actionCreateProcessing = (isProcessing = true) => ({
  type: actionTypes.CREATE_ACTION_PROCESSING,
  payload: isProcessing,
});

export const createHost =
  ({ host, port, username, alias, onSuccess = () => {} }) =>
  async (dispatch, getState, { api }) => {
    dispatch(actionCreateProcessing());
    const response = await api.hosts.createHost({
      host,
      port,
      username,
      alias,
    });
    if (response.success) {
      if (onSuccess) onSuccess();
      notify({
        title: 'Host',
        description: 'Host was created',
        status: status.SUCCESS,
      });
      dispatch(getHosts());
    }
    dispatch(actionCreateProcessing(false));
  };

const updateActionProcessing = (isProcessing = true) => ({
  type: actionTypes.UPDATE_ACTION_PROCESSING,
  payload: isProcessing,
});

export const updateHost =
  ({ port, username, alias, id, host, onSuccess }) =>
  async (dispatch, getState, { api }) => {
    dispatch(updateActionProcessing());
    const response = await api.hosts.updateHost({
      id,
      port,
      host,
      username,
      alias,
    });
    if (response.success) {
      if (onSuccess) onSuccess();
      notify({
        title: 'Host',
        description: 'Host was updated',
        status: status.SUCCESS,
      });
    }
    dispatch(updateActionProcessing(false));
  };

export const deleteHost =
  ({ id, callback = () => {} }) =>
  async (dispatch, getState, { api }) => {
    // TODO: add actionProcessing to each host
    const response = await api.hosts.deleteHost(id);
    if (response.success) {
      dispatch({ type: actionTypes.DELETE_HOST, payload: id });
      if (callback) callback();
      notify({
        title: 'Host',
        description: 'Host was deleted',
        status: status.SUCCESS,
      });
    }
  };
