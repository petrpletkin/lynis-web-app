import { initialState } from '../initial';
import { actionTypes } from '../actions';

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ACTION_PROCESSING:
      return {
        ...state,
        actionProcessing: action.payload,
      };

    case actionTypes.UPDATE_ACTION_PROCESSING:
      return {
        ...state,
        updateActionProcessing: action.payload,
      };

    case actionTypes.ACTION_FAILURE:
      return {
        ...state,
        actionProcessing: false,
      };

    case actionTypes.GET_HOSTS:
      return {
        ...state,
        actionProcessing: false,
        hosts: action.payload,
      };

    case actionTypes.GET_HOST:
      return {
        ...state,
        actionProcessing: false,
        host: action.payload,
      };

    case actionTypes.CLEAR_HOST:
      return {
        ...state,
        host: null,
      };

    case actionTypes.DELETE_HOST: {
      const index = state.hosts.findIndex(({ id }) => id === action.payload);
      const hosts = [
        ...state.hosts.slice(0, index),
        ...state.hosts.slice(index + 1),
      ];
      return {
        ...state,
        hosts,
      };
    }

    case actionTypes.CREATE_ACTION_PROCESSING:
      return {
        ...state,
        createActionProcessing: action.payload,
      };

    default:
      return state;
  }
};
