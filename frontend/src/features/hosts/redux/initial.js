export const initialState = {
  actionProcessing: false,
  createActionProcessing: false,
  updateActionProcessing: false,
  hosts: [],
  host: null,
};
