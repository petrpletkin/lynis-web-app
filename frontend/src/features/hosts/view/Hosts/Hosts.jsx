import React, { useState, useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { Link as RouterLink } from 'react-router-dom';

import { actions as hostsActions } from 'features/hosts';
import { actions as reportsActions } from 'features/reports';

import { ImPlay2 } from 'react-icons/im';

import { useAlertContext } from 'shared/context/AlertContext';

import { getProgressBarColorScheme } from 'shared/utils/getProgressBarColorScheme';

import {
  DeleteIcon,
  RepeatIcon,
  AddIcon,
  ChevronDownIcon,
  EditIcon,
} from '@chakra-ui/icons';
import {
  Box,
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  Heading,
  Progress,
  Link,
  Text,
  IconButton,
  Menu,
  MenuButton,
  Button,
  MenuList,
  MenuItem,
  Icon,
} from '@chakra-ui/react';

import WithSpinner from 'shared/components/WithSpinner';

import AddHostModal from './AddHostModal';
import EditHostModal from '../EditHostModal';

const Hosts = () => {
  const dispatch = useDispatch();
  const getHosts = useCallback(() => {
    dispatch(hostsActions.getHosts());
  }, [dispatch]);

  const { openAlert } = useAlertContext();

  const deleteHost = useCallback(
    (id) => {
      openAlert({
        title: 'Delete Host',
        successButtonText: 'Delete',
        successButtonColorScheme: 'red',
        onSuccess: () => dispatch(hostsActions.deleteHost({ id })),
      });
    },
    [dispatch, openAlert]
  );

  useEffect(() => {
    getHosts();
  }, [getHosts]);

  const hosts = useSelector((state) => state.hosts.hosts, shallowEqual);

  const actionProcessing = useSelector(
    (state) => state.hosts.actionProcessing,
    shallowEqual
  );

  const createReport = useCallback(() => {
    dispatch(
      reportsActions.createReport({
        reports: hosts
          .filter(({ isRunningNow }) => !isRunningNow)
          .map(({ host, username, port }) => ({
            host,
            username,
            port,
            alias: '',
          })),
        callback: getHosts,
      })
    );
  }, [dispatch, getHosts, hosts]);

  const [info, setInfo] = useState(null);

  const [isOpen, setIsOpen] = useState(false);
  const openEditModal = useCallback((value) => {
    setInfo(value);
    setIsOpen(true);
  }, []);
  const closeEditModal = useCallback(() => setIsOpen(false), []);

  const hostsList = useMemo(
    () =>
      hosts.map(
        ({
          id,
          host,
          port,
          username,
          alias,
          lastUpdateDate,
          suggestions,
          warnings,
          osFullname,
          isRunningNow,
          hardeningIndex,
          reports,
        }) => (
          <Tr>
            <Td>
              <Link as={RouterLink} key={id} to={`/hosts/${id}`}>
                {alias}
              </Link>
            </Td>
            <Td textAlign="center">
              <Link as={RouterLink} key={id} to={`/hosts/${id}`}>
                {`${host}:${port}`}
              </Link>
            </Td>
            <Td textAlign="center">{osFullname}</Td>
            <Td textAlign="center">{warnings}</Td>
            <Td textAlign="center">{suggestions}</Td>
            <Td>
              <Box display="flex" flexDirection="column" alignItems="center">
                <Progress
                  max="100"
                  colorScheme={getProgressBarColorScheme(hardeningIndex)}
                  w="100%"
                  value={hardeningIndex}
                />
                <Text>{hardeningIndex}</Text>
              </Box>
            </Td>
            <Td textAlign="center">{lastUpdateDate}</Td>
            <Td>
              <Menu spacing={3}>
                <MenuButton
                  as={Button}
                  rightIcon={<ChevronDownIcon w="5" />}
                  disabled={reports.length === 0}
                >
                  Reports
                </MenuButton>
                <MenuList maxH="400px" overflow="auto">
                  {reports.map(({ id, createdDate }) => (
                    <MenuItem key={id} as={RouterLink} to={`/reports/${id}`}>
                      {createdDate}
                    </MenuItem>
                  ))}
                </MenuList>
              </Menu>
            </Td>
            <Td>
              <IconButton
                aria-label="Update host"
                onClick={() =>
                  openEditModal({
                    id,
                    host,
                    username,
                    port,
                    alias,
                  })
                }
                colorScheme="gray"
                icon={<EditIcon />}
              />
            </Td>
            <Td>
              <IconButton
                onClick={() => deleteHost(id)}
                aria-label="Remove host"
                colorScheme="red"
                disabled={isRunningNow}
                icon={<DeleteIcon />}
              />
            </Td>
          </Tr>
        )
      ),
    [deleteHost, hosts, openEditModal]
  );

  const [isAddHostModalOpen, setIsAddHostModalOpen] = useState(false);
  const openAddHostModal = useCallback(() => setIsAddHostModalOpen(true), []);
  const closeAddHostModal = useCallback(() => setIsAddHostModalOpen(false), []);

  return (
    <>
      <AddHostModal isOpen={isAddHostModalOpen} onClose={closeAddHostModal} />
      {isOpen && (
        <EditHostModal
          isOpen={isOpen}
          onClose={closeEditModal}
          onSuccess={getHosts}
          info={info}
        />
      )}
      <Box p="10">
        <Box display="flex" alignItems="center" mb="7">
          <Heading as="h2" size="xl" mr="auto">
            Hosts
          </Heading>
          <IconButton
            aria-label="Open add host modal"
            onClick={openAddHostModal}
            colorScheme="green"
            icon={<AddIcon />}
            mr="3"
          />
          <IconButton
            aria-label="Start report"
            onClick={createReport}
            colorScheme="gray"
            icon={<Icon as={ImPlay2} />}
            mr="3"
          />
          <IconButton
            aria-label="Refresh hosts"
            onClick={getHosts}
            colorScheme="gray"
            icon={<RepeatIcon />}
          />
        </Box>
        <Box>
          <WithSpinner isProcessing={actionProcessing}>
            <Box
              border="1px"
              borderColor="gray.300"
              borderRadius="2xl"
              overflow="auto"
            >
              <Table variant="striped" colorScheme="gray">
                <Thead>
                  <Tr>
                    <Th>Alias</Th>
                    <Th textAlign="center">Host:port</Th>
                    <Th textAlign="center">OS</Th>
                    <Th textAlign="center">Warnings</Th>
                    <Th textAlign="center">Suggestions</Th>
                    <Th textAlign="center">Hardening index</Th>
                    <Th textAlign="center">Last updated</Th>
                    <Th />
                    <Th />
                    <Th />
                  </Tr>
                </Thead>
                <Tbody>{hostsList}</Tbody>
                <Tfoot>
                  <Tr>
                    <Th />
                    <Th />
                    <Th />
                    <Th />
                    <Th />
                    <Th />
                    <Th />
                    <Th />
                    <Th />
                    <Th isNumeric>Total: {hosts.length}</Th>
                  </Tr>
                </Tfoot>
              </Table>
            </Box>
          </WithSpinner>
        </Box>
      </Box>
    </>
  );
};

export default React.memo(Hosts);
