import React, { useCallback, useMemo, useState } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import PropTypes from 'prop-types';

import { actions as hostsActions } from 'features/hosts';

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  Button,
  FormControl,
  FormLabel,
  Input,
} from '@chakra-ui/react';

const AddHostModal = ({ isOpen, onClose }) => {
  const [username, setUsername] = useState('');
  const changeUsername = useCallback((e) => setUsername(e.target.value), []);

  const [host, setHost] = useState('');
  const changeHost = useCallback((e) => setHost(e.target.value), []);

  const [port, setPort] = useState('22');
  const changePort = useCallback((e) => setPort(e.target.value), []);

  const [alias, setAlias] = useState('');
  const changeAlias = useCallback((e) => setAlias(e.target.value), []);

  const closeModal = useCallback(() => {
    onClose();
    setUsername('');
    setHost('');
    setPort('22');
    setAlias('');
  }, [onClose]);

  const dispatch = useDispatch();
  const onSubmit = useCallback(
    (e) => {
      e.preventDefault();
      dispatch(
        hostsActions.createHost({
          host,
          port: +port,
          username,
          alias,
          onSuccess: closeModal,
        })
      );
    },
    [alias, closeModal, dispatch, host, port, username]
  );

  const disabled = useMemo(
    () => host.length === 0 || username.length === 0 || port.length === 0,
    [host.length, port.length, username.length]
  );

  const actionProcessing = useSelector(
    (state) => state.hosts.createActionProcessing,
    shallowEqual
  );

  return (
    <Modal isOpen={isOpen} onClose={closeModal}>
      <ModalOverlay />
      <ModalContent>
        <form onSubmit={onSubmit}>
          <ModalHeader>Create Host</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <FormControl mb="2">
              <FormLabel>Username</FormLabel>
              <Input
                disabled={actionProcessing}
                type="text"
                value={username}
                onChange={changeUsername}
              />
            </FormControl>
            <FormControl mb="2">
              <FormLabel>Alias</FormLabel>
              <Input
                disabled={actionProcessing}
                type="text"
                value={alias}
                onChange={changeAlias}
              />
            </FormControl>
            <FormControl mb="2">
              <FormLabel>Host</FormLabel>
              <Input
                disabled={actionProcessing}
                type="text"
                value={host}
                onChange={changeHost}
              />
            </FormControl>
            <FormControl mb="2">
              <FormLabel>Port</FormLabel>
              <Input
                disabled={actionProcessing}
                type="text"
                value={port}
                onChange={changePort}
              />
            </FormControl>
          </ModalBody>
          <ModalFooter>
            <Button mr={3} onClick={closeModal}>
              Close
            </Button>
            <Button
              colorScheme="green"
              type="submit"
              disabled={disabled}
              isLoading={actionProcessing}
            >
              Create
            </Button>
          </ModalFooter>
        </form>
      </ModalContent>
    </Modal>
  );
};

AddHostModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default React.memo(AddHostModal);
