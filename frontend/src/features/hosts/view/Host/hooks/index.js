import { useMemo } from 'react';
import { useSelector, shallowEqual } from 'react-redux';

import { Text, Progress, Switch, Box } from '@chakra-ui/react';

import { getProgressBarColorScheme } from 'shared/utils/getProgressBarColorScheme';

export const useMainHostInfo = () => {
  const host = useSelector((state) => state.hosts.host, shallowEqual);

  const mainInfo = useMemo(() => {
    if (!host) return [];
    return [
      {
        label: 'Id',
        value: <Text>{host.id}</Text>,
      },
      {
        label: 'Host',
        value: <Text>{host.host}</Text>,
      },
      {
        label: 'Port',
        value: <Text>{host.port}</Text>,
      },
      {
        label: 'Alias',
        value: <Text>{host.alias}</Text>,
      },
      {
        label: 'OS',
        value: <Text>{host.osFullname}</Text>,
      },
      {
        label: 'Username',
        value: <Text>{host.username}</Text>,
      },
      {
        label: 'Created date',
        value: <Text>{host.createdDate}</Text>,
      },
      {
        label: 'Last updated date',
        value: <Text>{host.lastUpdateDate}</Text>,
      },
      {
        label: 'Is running now',
        value: <Switch isChecked={host.isRunningNow} />,
      },
      {
        label: 'Hardening index',
        value: (
          <Box display="flex" alignItems="center" flexGrow="1">
            <Text mr="1">({host.hardeningIndex})</Text>
            <Progress
              colorScheme={getProgressBarColorScheme(host.hardeningIndex)}
              max="100"
              w="100%"
              value={host.hardeningIndex}
            />
          </Box>
        ),
      },
    ];
  }, [host]);

  return {
    mainInfo,
    host,
  };
};
