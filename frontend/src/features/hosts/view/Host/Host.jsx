import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { useHistory, useParams, Link as RouteLink } from 'react-router-dom';

import { actions as hostsActions } from 'features/hosts';
import { actions as reportsActions } from 'features/reports';

import { ImPlay2 } from 'react-icons/im';

import { useAlertContext } from 'shared/context/AlertContext';

import {
  InfoIcon,
  WarningIcon,
  RepeatIcon,
  DeleteIcon,
  ChevronDownIcon,
  ChevronRightIcon,
  EditIcon,
} from '@chakra-ui/icons';
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from 'recharts';
import {
  Box,
  Heading,
  IconButton,
  Table,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  Wrap,
  WrapItem,
  Menu,
  MenuButton,
  Button,
  MenuList,
  MenuItem,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Icon,
} from '@chakra-ui/react';

import WithSpinner from 'shared/components/WithSpinner';
import EditHostModal from '../EditHostModal';

import { useMainHostInfo } from './hooks';

const Host = () => {
  const { id } = useParams();

  const dispatch = useDispatch();
  const getHost = useCallback(() => {
    dispatch(hostsActions.getHost(id));
  }, [dispatch, id]);
  const clearHost = useCallback(() => {
    dispatch(hostsActions.clearHost());
  }, [dispatch]);

  useEffect(() => {
    getHost();
    return clearHost;
  }, [clearHost, getHost]);

  const actionProcessing = useSelector(
    (state) => state.hosts.actionProcessing,
    shallowEqual
  );

  const { mainInfo, host } = useMainHostInfo();

  const history = useHistory();

  const { openAlert } = useAlertContext();

  const deleteHost = useCallback(() => {
    openAlert({
      title: 'Delete Host',
      successButtonText: 'Delete',
      successButtonColorScheme: 'red',
      onSuccess: () =>
        dispatch(
          hostsActions.deleteHost({
            id,
            callback: () => history.push('/hosts'),
          })
        ),
    });
  }, [dispatch, history, id, openAlert]);

  const data = useMemo(() => {
    if (!host) return [];
    return (
      host.reports
        ?.map(({ shortDate, suggestions, warnings }) => ({
          date: shortDate,
          suggestions,
          warnings,
        }))
        ?.reverse() || []
    );
  }, [host]);

  const createReport = useCallback(() => {
    if (host) {
      dispatch(
        reportsActions.createReport({
          reports: [
            {
              host: host.host,
              username: host.username,
              port: host.port,
              alias: '',
            },
          ],
          callback: getHost,
        })
      );
    }
  }, [dispatch, getHost, host]);

  const [isOpen, setIsOpen] = useState(false);
  const openEditModal = useCallback(() => {
    if (host) setIsOpen(true);
  }, [host]);
  const closeEditModal = useCallback(() => setIsOpen(false), []);

  const editInfo = useMemo(
    () => ({
      id: host?.id,
      host: host?.host || '',
      username: host?.username || '',
      port: host?.port || 0,
      alias: host?.alias || '',
    }),
    [host]
  );

  return (
    <>
      {isOpen && (
        <EditHostModal
          isOpen={isOpen}
          onSuccess={getHost}
          onClose={closeEditModal}
          info={editInfo}
        />
      )}
      <Box p="10" minH="100%" display="flex" flexDir="column">
        <Breadcrumb separator={<ChevronRightIcon color="gray.500" />} mb="5">
          <BreadcrumbItem>
            <BreadcrumbLink as={RouteLink} to={'/hosts'}>
              Hosts
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbItem isCurrentPage>
            <BreadcrumbLink as={RouteLink} to={`/hosts/${id}`}>
              {id}
            </BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>
        <Box display="flex" alignItems="center" mb="7">
          <Heading as="h2" size="xl" mr="auto">
            Host
          </Heading>
          {host && (
            <IconButton
              aria-label="Start report"
              onClick={createReport}
              colorScheme="gray"
              disabled={host?.isRunningNow || false}
              icon={<Icon as={ImPlay2} />}
              mr="3"
            />
          )}
          <IconButton
            aria-label="Refresh host"
            onClick={getHost}
            colorScheme="gray"
            icon={<RepeatIcon />}
            mr="3"
          />
          <IconButton
            aria-label="Update host"
            onClick={openEditModal}
            colorScheme="gray"
            icon={<EditIcon />}
            mr="3"
          />
          <IconButton
            onClick={deleteHost}
            aria-label="Remove host"
            colorScheme="red"
            disabled={host?.isRunningNow || false}
            icon={<DeleteIcon />}
          />
        </Box>
        <WithSpinner isProcessing={actionProcessing}>
          {host && (
            <>
              <Box mb="10">
                <Box display="flex" mb="2">
                  <Heading as="h4" size="md" mb="7" mr="auto">
                    System overview
                  </Heading>
                  <Menu spacing={3}>
                    <MenuButton
                      as={Button}
                      rightIcon={<ChevronDownIcon w="5" />}
                      disabled={host.reports.length === 0}
                    >
                      Reports
                    </MenuButton>
                    <MenuList maxH="400px" overflow="auto">
                      {host.reports.map(({ id, createdDate }) => (
                        <MenuItem key={id} as={RouteLink} to={`/reports/${id}`}>
                          {createdDate}
                        </MenuItem>
                      ))}
                    </MenuList>
                  </Menu>
                </Box>
                <Box border="1px" borderColor="gray.300" overflow="hidden">
                  <Heading as="h5" size="md" variant="bgGray" p="3">
                    Main info
                  </Heading>
                  <Wrap p="3">
                    {mainInfo.map(({ label, value }) => {
                      return (
                        <WrapItem
                          key={label}
                          w="30%"
                          display="flex"
                          alignItems="center"
                        >
                          <Text fontWeight="bold" mr="1">
                            {`${label}:`}
                          </Text>
                          {value}
                        </WrapItem>
                      );
                    })}
                  </Wrap>
                </Box>
              </Box>
              <Box>
                <Heading as="h4" size="md" mb="7">
                  Security Hardening
                </Heading>
                <Box mb="5">
                  <LineChart width={750} height={300} data={data}>
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="date" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Line
                      type="monotone"
                      dataKey="suggestions"
                      stroke="#48BB78"
                    />
                    <Line
                      type="monotone"
                      dataKey="warnings"
                      activeDot={{ r: 8 }}
                      stroke="#E53E3E"
                    />
                  </LineChart>
                </Box>
                <Box
                  border="1px"
                  borderColor="gray.300"
                  mb="10"
                  overflow="auto"
                >
                  <Table colorScheme="gray" variant="striped">
                    <Thead>
                      <Tr>
                        <Th>Warnings</Th>
                        <Th isNumeric>Test</Th>
                      </Tr>
                    </Thead>
                    <Tbody>
                      {host.warnings.map(({ description, test }, index) => (
                        <Tr key={index}>
                          <Td>
                            <Box display="flex" alignItems="center">
                              <WarningIcon mr="1.5" color="red.500" />
                              {description}
                            </Box>
                          </Td>
                          <Td isNumeric>{test}</Td>
                        </Tr>
                      ))}
                    </Tbody>
                  </Table>
                </Box>
                <Box border="1px" borderColor="gray.300" overflow="auto">
                  <Table colorScheme="gray" variant="striped">
                    <Thead>
                      <Tr>
                        <Th>Suggestions</Th>
                        <Th>Severity</Th>
                        <Th textAlign="center">Test</Th>
                      </Tr>
                    </Thead>
                    <Tbody>
                      {host.suggestions.map(
                        ({ id, description, severity }, index) => (
                          <Tr key={index}>
                            <Td>
                              <Box display="flex" alignItems="center">
                                <InfoIcon mr="1.5" color="green.500" />
                                {description}
                              </Box>
                            </Td>
                            <Td textAlign="center">{severity}</Td>
                            <Td isNumeric>{id}</Td>
                          </Tr>
                        )
                      )}
                    </Tbody>
                  </Table>
                </Box>
              </Box>
            </>
          )}
        </WithSpinner>
      </Box>
    </>
  );
};

export default React.memo(Host);
