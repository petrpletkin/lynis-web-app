import React, { useCallback, useMemo, useState } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import PropTypes from 'prop-types';

import { actions as hostsActions } from 'features/hosts';

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  Button,
  FormControl,
  FormLabel,
  Input,
} from '@chakra-ui/react';

const EditHostModal = ({ isOpen, onSuccess, onClose, info }) => {
  const [username, setUsername] = useState(info.username || '');
  const changeUsername = useCallback((e) => setUsername(e.target.value), []);

  const [port, setPort] = useState(info.port || '');
  const changePort = useCallback((e) => setPort(e.target.value), []);

  const [alias, setAlias] = useState(info.alias || '');
  const changeAlias = useCallback((e) => setAlias(e.target.value), []);

  const dispatch = useDispatch();
  const onSubmit = useCallback(
    (e) => {
      e.preventDefault();
      dispatch(
        hostsActions.updateHost({
          id: info.id,
          port: +port,
          host: info.host,
          username,
          alias,
          onSuccess: () => {
            onSuccess();
            onClose();
          },
        })
      );
    },
    [alias, dispatch, info.host, info.id, onClose, onSuccess, port, username]
  );

  const disabled = useMemo(
    () => alias.length === 0 || username.length === 0 || port.length === 0,
    [alias.length, port.length, username.length]
  );

  const actionProcessing = useSelector(
    (state) => state.hosts.updateActionProcessing,
    shallowEqual
  );

  const closeModal = useCallback(() => onClose(), [onClose]);

  return (
    <Modal isOpen={isOpen} onClose={closeModal}>
      <ModalOverlay />
      <ModalContent>
        <form onSubmit={onSubmit}>
          <ModalHeader>Update Host</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <FormControl mb="2">
              <FormLabel>Username</FormLabel>
              <Input
                disabled={actionProcessing}
                type="text"
                value={username}
                onChange={changeUsername}
              />
            </FormControl>
            <FormControl mb="2">
              <FormLabel>Alias</FormLabel>
              <Input
                disabled={actionProcessing}
                type="text"
                value={alias}
                onChange={changeAlias}
              />
            </FormControl>
            <FormControl mb="2">
              <FormLabel>Port</FormLabel>
              <Input
                disabled={actionProcessing}
                type="text"
                value={port}
                onChange={changePort}
              />
            </FormControl>
          </ModalBody>
          <ModalFooter>
            <Button mr={3} onClick={closeModal}>
              Close
            </Button>
            <Button
              colorScheme="green"
              type="submit"
              disabled={disabled}
              isLoading={actionProcessing}
            >
              Update
            </Button>
          </ModalFooter>
        </form>
      </ModalContent>
    </Modal>
  );
};

EditHostModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  info: PropTypes.shape({
    id: PropTypes.string.isRequired,
    host: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
    port: PropTypes.number.isRequired,
    alias: PropTypes.string.isRequired,
  }).isRequired,
};

export default React.memo(EditHostModal);
