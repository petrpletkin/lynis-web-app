import { useMemo } from 'react';

import { Text, Switch } from '@chakra-ui/react';

export const useMainInfo = ({
  firewallInstalled,
  firewallEmptyRuleset,
  packageAuditTool,
  packageAuditToolFound,
  authenticationTwoFactorEnabled,
  authenticationTwoFactorRequired,
  ldapAuthEnabled,
  ldapPamEnabled,
  passwordMaxDays,
  passwordMinDays,
  automationToolPresent,
  pamPwquality,
  compilerInstalled,
  authFailedLoginsLogged,
  apparmorEnabled,
  apparmorPolicyLoaded,
  authGroupIdsUnique,
  authGroupNamesUnique,
}) =>
  useMemo(
    () => [
      {
        label: 'Firewall installed',
        value: <Switch isChecked={firewallInstalled} />,
      },
      {
        label: 'Firewall empty ruleset',
        value: <Switch isChecked={firewallEmptyRuleset} />,
      },
      {
        label: 'Package audit tool',
        value: <Text>{packageAuditTool}</Text>,
      },
      {
        label: 'Package audit tool found',
        value: <Switch isChecked={packageAuditToolFound} />,
      },
      {
        label: 'Authentication two factor enabled',
        value: <Switch isChecked={authenticationTwoFactorEnabled} />,
      },
      {
        label: 'Authentication two factor required',
        value: <Switch isChecked={authenticationTwoFactorRequired} />,
      },
      {
        label: 'LDAP auth enabled',
        value: <Switch isChecked={ldapAuthEnabled} />,
      },
      {
        label: 'LDAP PAM enabled',
        value: <Switch isChecked={ldapPamEnabled} />,
      },
      {
        label: 'Password max days',
        value: <Text>{passwordMaxDays}</Text>,
      },
      {
        label: 'Password min days',
        value: <Text>{passwordMinDays}</Text>,
      },
      {
        label: 'Automation tool present',
        value: <Switch isChecked={automationToolPresent} />,
      },
      {
        label: 'PAM password quality',
        value: <Text>{pamPwquality}</Text>,
      },
      {
        label: 'Compiler installed',
        value: <Switch isChecked={compilerInstalled} />,
      },
      {
        label: 'Auth failed logins logged',
        value: <Switch isChecked={authFailedLoginsLogged} />,
      },
      {
        label: 'AppArmor enabled',
        value: <Switch isChecked={apparmorEnabled} />,
      },
      {
        label: 'AppArmor policy loaded',
        value: <Switch isChecked={apparmorPolicyLoaded} />,
      },
      {
        label: 'Auth group ids unique',
        value: <Switch isChecked={authGroupIdsUnique} />,
      },
      {
        label: 'Auth group names uique',
        value: <Switch isChecked={authGroupNamesUnique} />,
      },
    ],
    [
      apparmorEnabled,
      apparmorPolicyLoaded,
      authFailedLoginsLogged,
      authGroupIdsUnique,
      authGroupNamesUnique,
      authenticationTwoFactorEnabled,
      authenticationTwoFactorRequired,
      automationToolPresent,
      compilerInstalled,
      firewallEmptyRuleset,
      firewallInstalled,
      ldapAuthEnabled,
      ldapPamEnabled,
      packageAuditTool,
      packageAuditToolFound,
      pamPwquality,
      passwordMaxDays,
      passwordMinDays,
    ]
  );
