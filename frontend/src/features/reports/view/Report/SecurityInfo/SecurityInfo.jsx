import React from 'react';

import { useIsDataEmpty } from 'shared/hooks/useIsDataEmpty';

import { Box, Heading } from '@chakra-ui/react';

import InfoBlock from 'shared/components/InfoBlock';
import Table from 'shared/components/Table';

import { useMainInfo } from './hooks/useMainInfo';

const SecurityInfo = ({ data }) => {
  const mainInfo = useMainInfo(data);

  const isDataEmpty = useIsDataEmpty(
    mainInfo,
    data.firewallSoftwares,
    data.packageManagers,
    data.automationToolsRunning,
    data.compilers,
    data.idsIpsTooling,
    data.fail2banConfigs,
    data.fail2banEnabledServices,
    data.realUsers,
    data.homeDirectories
  );
  if (isDataEmpty) return null;

  return (
    <Box mb="10" display="flex" flexDir="column">
      <Heading as="h5" size="md" mb="5">
        Security info:
      </Heading>

      <InfoBlock title="Main info" items={mainInfo} />

      <Table
        title="Firewall softwares"
        heads={[{ value: 'Software' }]}
        rows={data.firewallSoftwares}
      />

      <Table
        title="Package managers"
        heads={[{ value: 'Package manager' }]}
        rows={data.packageManagers}
      />

      <Table
        title="Automation tools running"
        heads={[{ value: 'Tool' }]}
        rows={data.automationToolsRunning}
      />

      <Table
        title="Compilers"
        heads={[{ value: 'Compiler' }]}
        rows={data.compilers}
      />

      <Table
        title="Ids IPs tooling"
        heads={[{ value: 'Id' }]}
        rows={data.idsIpsTooling}
      />

      <Table
        title="Fail2ban configs"
        heads={[{ value: 'Config' }]}
        rows={data.fail2banConfigs}
      />

      <Table
        title="Fail2ban enabled services"
        heads={[{ value: 'Service' }]}
        rows={data.fail2banEnabledServices}
      />

      <Table
        title="Real users"
        heads={[{ value: 'Name' }, { value: 'UID' }]}
        rows={data.realUsers}
      />

      <Table
        title="Home directories"
        heads={[{ value: 'Directory' }]}
        rows={data.homeDirectories}
        isLast
      />
    </Box>
  );
};

export default React.memo(SecurityInfo);
