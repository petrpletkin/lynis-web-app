import { useMemo } from 'react';

import { Text, Switch } from '@chakra-ui/react';

export const useMainHostInfo = ({
  hostname,
  domainName,
  osFullname,
  osKernelVersionFull,
  packageAuditTool,
  packageAuditToolFound,
  memorySize,
  selinux,
  cpuPae,
  cpuNx,
  uptimeInDays,
  uptimeInSeconds,
  locateDb,
  isNotebook,
  isContainer,
}) =>
  useMemo(
    () => [
      {
        label: 'Host name',
        value: <Text>{hostname}</Text>,
      },
      {
        label: 'Domain name',
        value: <Text>{domainName}</Text>,
      },
      {
        label: 'OS fullname',
        value: <Text>{osFullname}</Text>,
      },
      {
        label: 'OS kernel version full',
        value: <Text>{osKernelVersionFull}</Text>,
      },
      {
        label: 'Package audit tool',
        value: <Text>{packageAuditTool}</Text>,
      },
      {
        label: 'Package audit tool found',
        value: <Text>{packageAuditToolFound}</Text>,
      },
      {
        label: 'Memory size',
        value: <Text>{memorySize}</Text>,
      },
      {
        label: 'SELinux status',
        value: <Switch isChecked={selinux.status} />,
      },
      {
        label: 'SELinux mode',
        value: <Text>{selinux.mode}</Text>,
      },
      {
        label: 'CPU pae',
        value: <Text>{cpuPae}</Text>,
      },
      {
        label: 'CPU nx',
        value: <Text>{cpuNx}</Text>,
      },
      {
        label: 'Uptime in days',
        value: <Text>{uptimeInDays}</Text>,
      },
      {
        label: 'Uptime in seconds',
        value: <Text>{uptimeInSeconds}</Text>,
      },
      {
        label: 'Locate DB',
        value: <Text>{locateDb}</Text>,
      },
      {
        label: 'Is notebook',
        value: <Switch isChecked={isNotebook} />,
      },
      {
        label: 'Is container',
        value: <Switch isChecked={isContainer} />,
      },
    ],
    [
      cpuNx,
      cpuPae,
      domainName,
      hostname,
      isContainer,
      isNotebook,
      locateDb,
      memorySize,
      osFullname,
      osKernelVersionFull,
      packageAuditTool,
      packageAuditToolFound,
      selinux.mode,
      selinux.status,
      uptimeInDays,
      uptimeInSeconds,
    ]
  );
