import { useMemo } from 'react';

import { Text, Switch } from '@chakra-ui/react';

export const useLoggingInfo = ({ logRotationConfigFound, logRotationTool }) =>
  useMemo(
    () => [
      {
        label: 'Log rotation tool',
        value: <Text>{logRotationTool}</Text>,
      },
      {
        label: 'Log rotation config found',
        value: <Switch isChecked={logRotationConfigFound} />,
      },
    ],
    [logRotationConfigFound, logRotationTool]
  );
