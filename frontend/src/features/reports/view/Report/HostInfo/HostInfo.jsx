import React from 'react';

import { useIsDataEmpty } from 'shared/hooks/useIsDataEmpty';

import { Box, Heading } from '@chakra-ui/react';

import InfoBlock from 'shared/components/InfoBlock';
import Table from 'shared/components/Table';

import { useMainHostInfo } from './hooks/useMainHostInfo';
import { useLoggingInfo } from './hooks/useLoggingInfo';

const HostInfo = ({ data }) => {
  const mainHostInfo = useMainHostInfo(data);
  const loggingInfo = useLoggingInfo(data.loggingInfo);

  const isDataEmpty = useIsDataEmpty(
    mainHostInfo,
    data.resolveConfSearchDomains,
    data.availableShells,
    data.binaryPaths,
    data.cronJobs,
    loggingInfo,
    data.loggingInfo.syslogDaemons,
    data.loggingInfo.logDirectories
  );

  if (isDataEmpty) return null;

  return (
    <Box mb="10" display="flex" flexDir="column">
      <Heading as="h5" size="md" mb="5">
        Host info:
      </Heading>

      <InfoBlock title="Main info" items={mainHostInfo} />

      <Table
        title="Resolve conf search domains"
        heads={[{ value: 'Domain' }]}
        rows={data.resolveConfSearchDomains}
      />

      <Table
        title="Available shells"
        heads={[{ value: 'Shell' }]}
        rows={data.availableShells}
      />

      <Table
        title="Binary paths"
        heads={[{ value: 'Path' }]}
        rows={data.binaryPaths}
      />

      <Table
        title="Cron jobs"
        heads={[{ value: 'Job' }]}
        rows={data.cronJobs}
      />

      <InfoBlock title="Logging info" items={loggingInfo} />

      <Table
        title="System log daemons"
        heads={[{ value: 'Daemon' }]}
        rows={data.loggingInfo.syslogDaemons}
      />

      <Table
        title="Log directories"
        heads={[{ value: 'Directory' }]}
        rows={data.loggingInfo.logDirectories}
        isLast
      />
    </Box>
  );
};

export default React.memo(HostInfo);
