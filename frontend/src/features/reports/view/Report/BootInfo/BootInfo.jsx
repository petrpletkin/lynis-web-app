import React from 'react';

import { useIsDataEmpty } from 'shared/hooks/useIsDataEmpty';

import { Box, Heading } from '@chakra-ui/react';

import InfoBlock from 'shared/components/InfoBlock';
import Table from 'shared/components/Table';

import { useMainInfo } from './hooks/useMainInfo';

const BootInfo = ({ data }) => {
  const mainInfo = useMainInfo(data);

  const isDataEmpty = useIsDataEmpty(mainInfo, data.servicesStartedAtBoot);
  if (isDataEmpty) return null;

  return (
    <Box display="flex" flexDir="column">
      <Heading as="h5" size="md" mb="5">
        Boot info:
      </Heading>

      <InfoBlock title="Main info" items={mainInfo} />

      <Table
        title="Services started at boot"
        heads={[{ value: 'Service' }]}
        rows={data.servicesStartedAtBoot}
        isLast
      />
    </Box>
  );
};

export default React.memo(BootInfo);
