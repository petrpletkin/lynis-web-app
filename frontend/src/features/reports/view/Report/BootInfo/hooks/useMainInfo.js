import { useMemo } from 'react';

import { Text, Switch } from '@chakra-ui/react';

export const useMainInfo = ({
  bootServiceTool,
  bootUefiBooted,
  bootUefiBootedSecure,
  linuxDefaultRunlevel,
  bootLoader,
}) =>
  useMemo(
    () => [
      {
        label: 'Boot service tool',
        value: <Text>{bootServiceTool}</Text>,
      },
      {
        label: 'Boot uefi booted',
        value: <Switch isChecked={bootUefiBooted} />,
      },
      {
        label: 'Boot uefi booted secure',
        value: <Switch isChecked={bootUefiBootedSecure} />,
      },
      {
        label: 'Linux default run level',
        value: <Text>{linuxDefaultRunlevel}</Text>,
      },
      {
        label: 'Boot loader',
        value: <Text>{bootLoader}</Text>,
      },
    ],
    [
      bootLoader,
      bootServiceTool,
      bootUefiBooted,
      bootUefiBootedSecure,
      linuxDefaultRunlevel,
    ]
  );
