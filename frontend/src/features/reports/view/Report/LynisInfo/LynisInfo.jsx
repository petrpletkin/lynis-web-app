import React, { useMemo } from 'react';

import { Box, Heading, Tr, Td } from '@chakra-ui/react';

import InfoBlock from 'shared/components/InfoBlock';
import Table from 'shared/components/Table';

import { useLynisInfo } from './hooks/useLynisInfo';

const heads = [
  {
    value: 'Name',
  },
  {
    value: 'Version',
    props: {
      isNumeric: true,
    },
  },
];

const LynisInfo = ({ data }) => {
  const lynisInfo = useLynisInfo(data);

  const trows = useMemo(
    () =>
      data.pluginEnabledPhases.map(({ name, version }, index) => (
        <Tr key={index}>
          <Td>{name}</Td>
          <Td isNumeric>{version}</Td>
        </Tr>
      )),
    [data.pluginEnabledPhases]
  );
  return (
    <Box mb="10" display="flex" flexDir="column">
      <Heading as="h5" size="md" mb="5">
        Lynis info:
      </Heading>

      <InfoBlock title="Main info" items={lynisInfo} />

      <Table title="Plugin enabled phases" heads={heads} trows={trows} isLast />
    </Box>
  );
};

export default React.memo(LynisInfo);
