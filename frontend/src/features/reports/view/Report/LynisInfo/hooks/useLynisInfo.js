import { useMemo } from 'react';

import { Text, Switch } from '@chakra-ui/react';

export const useLynisInfo = ({
  lynisVersion,
  lynisTestsDone,
  lynisUpdateAvailable,
  licenseKey,
  reportDatetimeStart,
  reportDatetimeEnd,
  reportVersionMajor,
  reportVersionMinor,
}) =>
  useMemo(
    () => [
      {
        label: 'Lynis version',
        value: <Text>{lynisVersion}</Text>,
      },
      {
        label: 'Lynis tests done',
        value: <Switch isChecked={lynisTestsDone} />,
      },
      {
        label: 'Lynis update available',
        value: <Switch isChecked={lynisUpdateAvailable} />,
      },
      {
        label: 'License key',
        value: <Text>{licenseKey}</Text>,
      },
      {
        label: 'Report datetime start',
        value: <Text>{reportDatetimeStart}</Text>,
      },
      {
        label: 'Report datetime end',
        value: <Text>{reportDatetimeEnd}</Text>,
      },
      {
        label: 'Report version major',
        value: <Text>{reportVersionMajor}</Text>,
      },
      {
        label: 'Report version minor',
        value: <Text>{reportVersionMinor}</Text>,
      },
    ],
    [
      licenseKey,
      lynisTestsDone,
      lynisUpdateAvailable,
      lynisVersion,
      reportDatetimeEnd,
      reportDatetimeStart,
      reportVersionMajor,
      reportVersionMinor,
    ]
  );
