import { useMemo } from 'react';

import { Box, Text, Progress } from '@chakra-ui/react';

import { getProgressBarColorScheme } from 'shared/utils/getProgressBarColorScheme';

export const useMainInfo = ({ hardeningIndex, auditor }) =>
  useMemo(
    () => [
      {
        label: 'Hardening index',
        value: (
          <Box display="flex" alignItems="center" flexGrow="1" mr="10">
            <Text mr="1">({hardeningIndex})</Text>
            <Progress
              colorScheme={getProgressBarColorScheme(hardeningIndex)}
              max="100"
              w="100%"
              maxW="200px"
              minW="150px"
              value={hardeningIndex}
            />
          </Box>
        ),
      },
      {
        label: 'Auditor',
        value: <Text>{auditor}</Text>,
      },
    ],
    [auditor, hardeningIndex]
  );
