import React from 'react';

import { useIsDataEmpty } from 'shared/hooks/useIsDataEmpty';

import { Box, Heading } from '@chakra-ui/react';

import InfoBlock from 'shared/components/InfoBlock';

import { useMainInfo } from './hooks/useMainInfo';

const HostFinding = ({ data }) => {
  const mainInfo = useMainInfo(data);

  const isDataEmpty = useIsDataEmpty(mainInfo);
  if (isDataEmpty) return null;

  return (
    <Box mb="10" display="flex" flexDir="column" alignItems="flex-start">
      <Heading as="h5" size="md" mb="5">
        Host findings:
      </Heading>
      <InfoBlock title="Hardening index" items={mainInfo} wItem="auto" isLast />
    </Box>
  );
};

export default React.memo(HostFinding);
