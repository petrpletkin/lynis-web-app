import React, { useMemo } from 'react';

import { useIsDataEmpty } from 'shared/hooks/useIsDataEmpty';

import { WarningIcon } from '@chakra-ui/icons';
import { Box, Tr, Td, Heading } from '@chakra-ui/react';

import Table from 'shared/components/Table';

const heads = [
  { value: 'Test Id' },
  { value: 'Description' },
  {
    value: 'Details',
    props: {
      textAlign: 'center',
    },
  },
  {
    value: 'Solution',
    props: {
      textAlign: 'center',
    },
  },
];

const Warnings = ({ items = [] }) => {
  const trows = useMemo(
    () =>
      items.map(({ description, test, details, solution }, index) => (
        <Tr key={index}>
          <Td>
            <Box display="flex" alignItems="center">
              <WarningIcon mr="1.5" color="red.500" />
              {test}
            </Box>
          </Td>
          <Td>{description}</Td>
          <Td textAlign="center">{details}</Td>
          <Td textAlign="center">{solution}</Td>
        </Tr>
      )),
    [items]
  );

  const isDataEmpty = useIsDataEmpty(items);
  if (isDataEmpty) return null;

  return (
    <Box mb="10">
      <Heading as="h5" size="md" mb="5">
        Warnings ({items.length}):
      </Heading>
      <Table heads={heads} trows={trows} isLast />
    </Box>
  );
};

export default React.memo(Warnings);
