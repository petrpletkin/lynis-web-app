import { useMemo } from 'react';

import { Text, Switch } from '@chakra-ui/react';

export const useMainInfo = ({
  ipv6Mode,
  ipv6Only,
  defaultGateway,
  nameCacheUsed,
}) =>
  useMemo(
    () => [
      {
        label: 'IPv6 mode',
        value: <Text>{ipv6Mode}</Text>,
      },
      {
        label: 'IPv6 only',
        value: <Switch isChecked={ipv6Only} />,
      },
      {
        label: 'Default gateway',
        value: <Text>{defaultGateway}</Text>,
      },
      {
        label: 'Name cache used',
        value: <Switch isChecked={nameCacheUsed} />,
      },
    ],
    [defaultGateway, ipv6Mode, ipv6Only, nameCacheUsed]
  );
