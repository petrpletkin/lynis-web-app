import React from 'react';

import { useIsDataEmpty } from 'shared/hooks/useIsDataEmpty';

import { Box, Heading } from '@chakra-ui/react';

import InfoBlock from 'shared/components/InfoBlock';
import Table from 'shared/components/Table';

import { useMainInfo } from './hooks/useMainInfo';

const NetworkInfo = ({ data }) => {
  const mainInfo = useMainInfo(data);

  const isDataEmpty = useIsDataEmpty(
    mainInfo,
    data.networkInterfaces,
    data.networkIpv4Addresses,
    data.networkIpv6Addresses,
    data.networkMacAddresses,
    data.nameServers,
    data.networkListenPorts,
    data.resolveConfSearchDomains
  );
  if (isDataEmpty) return null;

  return (
    <Box mb="10" display="flex" flexDir="column">
      <Heading as="h5" size="md" mb="5">
        Network info:
      </Heading>

      <InfoBlock title="Main info" items={mainInfo} />

      <Table
        title="Network interfaces"
        heads={[{ value: 'Interface' }]}
        rows={data.networkInterfaces}
      />

      <Table
        title="Network IPv4 addresses"
        heads={[{ value: 'IPv4' }]}
        rows={data.networkIpv4Addresses}
      />

      <Table
        title="Network IPv6 addresses"
        heads={[{ value: 'IPv6' }]}
        rows={data.networkIpv6Addresses}
      />

      <Table
        title="Network MAC addresses"
        heads={[{ value: 'MAC' }]}
        rows={data.networkMacAddresses}
      />

      <Table
        title="Name servers"
        heads={[{ value: 'Server' }]}
        rows={data.nameServers}
      />

      <Table
        title="Network listen ports"
        heads={[
          { value: 'Port' },
          { value: 'Owner process' },
          { value: 'Protocol' },
        ]}
        rows={data.networkListenPorts}
      />

      <Table
        title="Resolve conf search domains"
        heads={[{ value: 'Directory' }]}
        rows={data.resolveConfSearchDomains}
        isLast
      />
    </Box>
  );
};

export default React.memo(NetworkInfo);
