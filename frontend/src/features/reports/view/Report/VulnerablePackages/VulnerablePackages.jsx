import React from 'react';

import { useIsDataEmpty } from 'shared/hooks/useIsDataEmpty';

import { Box, Heading } from '@chakra-ui/react';

import Table from 'shared/components/Table';

const VulnerablePackages = ({ items = [] }) => {
  const isDataEmpty = useIsDataEmpty(items);
  if (isDataEmpty) return null;
  return (
    <Box mb="10">
      <Heading as="h5" size="md" mb="5">
        Vulnerable packages ({items.length}):
      </Heading>
      <Table heads={[{ value: 'Package' }]} rows={items} isLast />
    </Box>
  );
};

export default React.memo(VulnerablePackages);
