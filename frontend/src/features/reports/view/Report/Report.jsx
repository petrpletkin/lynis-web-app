import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { useParams, Link as RouteLink } from 'react-router-dom';

import { actions as reportsActions } from 'features/reports';

import { RepeatIcon, ChevronRightIcon } from '@chakra-ui/icons';
import {
  Box,
  Heading,
  IconButton,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
} from '@chakra-ui/react';

import WithSpinner from 'shared/components/WithSpinner';

import Warnings from './Warnings';
import Suggestions from './Suggestions';
import Manuals from './Manuals';
import HostFinding from './HostFinding';
import VulnerablePackages from './VulnerablePackages';
import LynisInfo from './LynisInfo';
import HostInfo from './HostInfo';
import NetworkInfo from './NetworkInfo';
import SecurityInfo from './SecurityInfo';
import BootInfo from './BootInfo';

const Report = () => {
  const { id } = useParams();

  const dispatch = useDispatch();
  const getReport = useCallback(() => {
    dispatch(reportsActions.getReport(id));
  }, [dispatch, id]);
  const clearReport = useCallback(() => {
    dispatch(reportsActions.clearReport());
  }, [dispatch]);

  useEffect(() => {
    getReport();
    return clearReport;
  }, [clearReport, getReport]);

  const actionProcessing = useSelector(
    (state) => state.reports.actionProcessing,
    shallowEqual
  );

  const report = useSelector((state) => state.reports.report, shallowEqual);

  return (
    <Box p="10" minH="100%" display="flex" flexDir="column">
      <Breadcrumb separator={<ChevronRightIcon color="gray.500" />} mb="5">
        <BreadcrumbItem>
          <BreadcrumbLink as={RouteLink} to={'/reports'}>
            Reports
          </BreadcrumbLink>
        </BreadcrumbItem>
        <BreadcrumbItem isCurrentPage>
          <BreadcrumbLink as={RouteLink} to={`/reports/${id}`}>
            {id}
          </BreadcrumbLink>
        </BreadcrumbItem>
      </Breadcrumb>
      <Box display="flex" alignItems="center" mb="7">
        <Heading as="h2" size="xl" mr="auto">
          Report
        </Heading>
        <IconButton
          aria-label="Refresh report"
          onClick={getReport}
          colorScheme="gray"
          icon={<RepeatIcon />}
          mr="3"
        />
      </Box>
      <WithSpinner isProcessing={actionProcessing}>
        {report && (
          <>
            <HostFinding data={report.hostFinding} />
            <Warnings items={report.warnings} />
            <Suggestions items={report.suggestions} />
            <Manuals items={report.manuals} />
            <VulnerablePackages items={report.vulnerablePackages} />
            <LynisInfo data={report.lynisInfo} />
            <HostInfo data={report.hostInfo} />
            <NetworkInfo data={report.networkInfo} />
            <SecurityInfo data={report.securityInfo} />
            <BootInfo data={report.bootInfo} />
          </>
        )}
      </WithSpinner>
    </Box>
  );
};

export default React.memo(Report);
