import React, { useMemo } from 'react';

import { useIsDataEmpty } from 'shared/hooks/useIsDataEmpty';

import { InfoIcon } from '@chakra-ui/icons';
import { Box, Td, Tr, Heading } from '@chakra-ui/react';

import Table from 'shared/components/Table';

const thead = [
  {
    value: 'Test Id',
  },
  {
    value: 'Description',
  },
  {
    value: 'Severity',
  },
];

const Suggestions = ({ items = [] }) => {
  const rows = useMemo(
    () =>
      items.map(({ id, description, severity }, index) => (
        <Tr key={index}>
          <Td>
            <Box display="flex" alignItems="center">
              <InfoIcon mr="1.5" color="green.500" />
              {id}
            </Box>
          </Td>
          <Td>{description}</Td>
          <Td textAlign="center">{severity}</Td>
        </Tr>
      )),
    [items]
  );

  const isDataEmpty = useIsDataEmpty(items);
  if (isDataEmpty) return null;

  return (
    <Box mb="10">
      <Heading as="h5" size="md" mb="5">
        Suggestions ({items.length}):
      </Heading>
      <Table heads={thead} trows={rows} isLast />
    </Box>
  );
};

export default React.memo(Suggestions);
