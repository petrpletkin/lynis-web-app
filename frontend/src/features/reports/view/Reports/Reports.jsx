import React, { useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { Link as RouterLink } from 'react-router-dom';

import { actions as reportsActions } from 'features/reports';

import { RepeatIcon } from '@chakra-ui/icons';
import {
  Box,
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  Heading,
  Link,
  IconButton,
} from '@chakra-ui/react';

import WithSpinner from 'shared/components/WithSpinner';
const Reports = () => {
  const dispatch = useDispatch();
  const getReports = useCallback(() => {
    dispatch(reportsActions.getReports());
  }, [dispatch]);

  useEffect(() => {
    getReports();
  }, [getReports]);

  const reports = useSelector((state) => state.reports.reports, shallowEqual);

  const actionProcessing = useSelector(
    (state) => state.reports.actionProcessing,
    shallowEqual
  );

  const reportsList = useMemo(
    () =>
      reports.map(({ id, host, createdDate }) => (
        <Tr key={id}>
          <Td>
            <Link as={RouterLink} to={`/reports/${id}`}>
              {id}
            </Link>
          </Td>
          <Td textAlign="center">{host}</Td>
          <Td isNumeric>{createdDate}</Td>
        </Tr>
      )),
    [reports]
  );

  return (
    <Box p="10">
      <Box display="flex" alignItems="center" mb="7">
        <Heading as="h2" size="xl" mr="auto">
          Reports
        </Heading>
        <IconButton
          aria-label="Refresh reports"
          onClick={getReports}
          colorScheme="gray"
          icon={<RepeatIcon />}
        />
      </Box>
      <Box>
        <WithSpinner isProcessing={actionProcessing}>
          <Box
            border="1px"
            borderColor="gray.300"
            borderRadius="2xl"
            overflow="auto"
          >
            <Table variant="striped" colorScheme="gray">
              <Thead>
                <Tr>
                  <Th>Id</Th>
                  <Th textAlign="center">Host</Th>
                  <Th isNumeric>Created date</Th>
                </Tr>
              </Thead>
              <Tbody>{reportsList}</Tbody>
              <Tfoot>
                <Tr>
                  <Th />
                  <Th />
                  <Th isNumeric>Total: {reportsList.length}</Th>
                </Tr>
              </Tfoot>
            </Table>
          </Box>
        </WithSpinner>
      </Box>
    </Box>
  );
};

export default React.memo(Reports);
