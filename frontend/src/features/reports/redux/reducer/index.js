import { initialState } from '../initial';
import { actionTypes } from '../actions';

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ACTION_PROCESSING:
      return {
        ...state,
        actionProcessing: action.payload,
      };

    case actionTypes.ACTION_FAILURE:
      return {
        ...state,
        actionProcessing: false,
      };

    case actionTypes.GET_REPORTS:
      return {
        ...state,
        actionProcessing: false,
        reports: action.payload,
      };

    case actionTypes.GET_REPORT:
      return {
        ...state,
        actionProcessing: false,
        report: action.payload,
      };

    case actionTypes.CLEAR_REPORT:
      return {
        ...state,
        report: null,
      };

    default:
      return state;
  }
};
