export const actionTypes = {
  ACTION_PROCESSING: 'reports/ACTION_PROCESSING',
  ACTION_FAILURE: 'reports/ACTION_FAILURE',
  GET_REPORTS: 'reports/GET_REPORTS',
  GET_REPORT: 'reports/GET_REPORT',
  CLEAR_REPORT: 'reports/CLEAR_REPORT',
};

const actionProcessing = (isProcessing = true) => ({
  type: actionTypes.ACTION_PROCESSING,
  payload: isProcessing,
});

const actionFailure = () => ({ type: actionTypes.ACTION_FAILURE });

export const getReports =
  () =>
  async (dispatch, getState, { api }) => {
    dispatch(actionProcessing());
    const response = await api.reports.getReports();
    if (response.success) {
      dispatch({ type: actionTypes.GET_REPORTS, payload: response.data });
    } else dispatch(actionFailure());
  };

export const getReport =
  (id) =>
  async (dispatch, getState, { api }) => {
    dispatch(actionProcessing());
    const response = await api.reports.getReport(id);
    if (response.success) {
      dispatch({ type: actionTypes.GET_REPORT, payload: response.data });
    } else dispatch(actionFailure());
  };

export const createReport =
  ({ reports, callback }) =>
  async (dispatch, getState, { api }) => {
    dispatch(actionProcessing());
    const response = await api.reports.createReport(reports);
    if (response.success) {
      if (callback) callback();
    } else dispatch(actionFailure());
  };

export const clearReport = () => ({ type: actionTypes.CLEAR_REPORT });
