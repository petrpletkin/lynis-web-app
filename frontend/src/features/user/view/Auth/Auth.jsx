import React, { useState, useCallback, useMemo } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import { actions as authActions } from 'features/user';

import {
  Center,
  FormControl,
  FormLabel,
  Input,
  Button,
  Box,
  Heading,
  useColorMode,
} from '@chakra-ui/react';

import ColorModeButton from 'shared/components/ColorModeButton';

const Auth = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const dispatch = useDispatch();
  const login = useCallback(() => {
    dispatch(authActions.login({ username, password }));
  }, [dispatch, password, username]);

  const changeUsername = useCallback((e) => setUsername(e.target.value), []);
  const changePassword = useCallback((e) => setPassword(e.target.value), []);

  const onSubmit = useCallback(
    (e) => {
      e.preventDefault();
      login();
    },
    [login]
  );

  const actionProcessing = useSelector(
    (state) => state.user.actionProcessing,
    shallowEqual
  );

  const disabled = useMemo(
    () => username === '' || password === '' || actionProcessing,
    [actionProcessing, password, username]
  );

  const { colorMode } = useColorMode();
  const bgColorBox = useMemo(
    () => (colorMode === 'dark' ? 'gray.700' : 'white'),
    [colorMode]
  );
  const bgColorCenter = useMemo(
    () => (colorMode === 'dark' ? '' : 'gray.50'),
    [colorMode]
  );

  return (
    <Center flexDirection="column" h="100%" w="100%" bgColor={bgColorCenter}>
      <Box pos="fixed" bottom="3" left="3">
        <ColorModeButton />
      </Box>
      <Box
        border="1px"
        borderColor="gray.200"
        p="10"
        borderRadius="2xl"
        bgColor={bgColorBox}
      >
        <form onSubmit={onSubmit}>
          <Heading as="h4" size="md" mb="5" textAlign="center">
            Authorization
          </Heading>
          <FormControl maxW="300px" mb="2">
            <FormLabel>Username</FormLabel>
            <Input type="text" value={username} onChange={changeUsername} />
          </FormControl>
          <FormControl maxW="300px" mb="5">
            <FormLabel>Password</FormLabel>
            <Input type="password" value={password} onChange={changePassword} />
          </FormControl>
          <Button
            type="submit"
            colorScheme="teal"
            w="100%"
            disabled={disabled}
            isLoading={actionProcessing}
          >
            Login
          </Button>
        </form>
      </Box>
    </Center>
  );
};

export default React.memo(Auth);
