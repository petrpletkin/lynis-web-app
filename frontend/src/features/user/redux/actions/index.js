import { AccessTokenManager } from 'shared/managers/AccessTokenManager';

export const actionTypes = {
  ACTION_PROCESSING: 'user/ACTION_PROCESSING',
  ACTION_FAILURE: 'user/ACTION_FAILURE',
  GET_INFO: 'user/GET_INFO',
  GET_TOKEN: 'user/GET_TOKEN',
  CLEAR_TOKEN: 'user/CLEAR_TOKEN',
  APP_ACTION_PROCESSING: 'auth/APP_ACTION_PROCESSING',
};

export const actionProcessing = (isProcessing = true) => ({
  type: actionTypes.ACTION_PROCESSING,
  payload: isProcessing,
});

export const getUserInfo =
  () =>
  async (dispatch, getState, { api }) => {
    dispatch(actionProcessing());
    const response = await api.user.getInfo();
    if (response.success) {
      dispatch({ type: actionTypes.GET_INFO, payload: response.data });
    } else dispatch({ type: actionTypes.ACTION_FAILURE });
  };

export const login =
  ({ username, password }) =>
  async (dispatch, getState, { api }) => {
    dispatch(actionProcessing());
    const response = await api.user.login({ username, password });
    if (response.success) {
      AccessTokenManager.setToken(response.data);
      dispatch({ type: actionTypes.GET_TOKEN, payload: response.data });
      dispatch(actionProcessing(false));
    } else dispatch({ type: actionTypes.ACTION_FAILURE });
  };

export const logout = () => {
  AccessTokenManager.setToken(null);
  return { type: actionTypes.CLEAR_TOKEN };
};

const appActionProcessing = (isProcessing = true) => ({
  type: actionTypes.APP_ACTION_PROCESSING,
  payload: isProcessing,
});

export const getToken = () => async (dispatch) => {
  dispatch(appActionProcessing());
  const token = AccessTokenManager.getToken();
  dispatch({ type: actionTypes.GET_TOKEN, payload: token });
  dispatch(appActionProcessing(false));
};
