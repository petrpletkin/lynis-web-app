import { initialState } from '../initial';
import { actionTypes } from '../actions';

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ACTION_PROCESSING:
      return {
        ...state,
        actionProcessing: action.payload,
      };

    case actionTypes.ACTION_FAILURE:
      return {
        ...state,
        actionProcessing: false,
        errorMessage: action.payload,
      };

    case actionTypes.GET_INFO: {
      const { id, username, email } = action.payload;
      return {
        ...state,
        actionProcessing: false,
        id,
        username,
        email,
      };
    }

    case actionTypes.GET_TOKEN:
      return {
        ...state,
        token: action.payload,
      };

    case actionTypes.CLEAR_TOKEN:
      return {
        ...state,
        token: initialState.token,
      };

    case actionTypes.APP_ACTION_PROCESSING:
      return {
        ...state,
        appActionProcessing: action.payload,
      };

    default:
      return state;
  }
};
