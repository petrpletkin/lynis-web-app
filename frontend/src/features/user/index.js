import { actions, reducer } from './redux';
import Auth from './view/Auth';

export { Auth, actions, reducer };
