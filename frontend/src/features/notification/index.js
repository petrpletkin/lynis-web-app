import { createStandaloneToast } from '@chakra-ui/react';

const toast = createStandaloneToast();

export const status = {
  INFO: 'info',
  WARNING: 'warning',
  SUCCESS: 'success',
  ERROR: 'error',
};

const initialAlert = {
  title: '',
  description: '',
  status: status.INFO,
  position: 'top-right',
  duration: 4000,
  onCloseComplete: () => {},
};

export const notify = ({
  title = initialAlert.title,
  description = initialAlert.description,
  status = initialAlert.status,
  position = initialAlert.position,
  duration = initialAlert.duration,
  onCloseComplete = initialAlert.onCloseComplete,
} = initialAlert) =>
  toast({
    title,
    description,
    status,
    duration,
    isClosable: true,
    position,
    onCloseComplete,
  });
