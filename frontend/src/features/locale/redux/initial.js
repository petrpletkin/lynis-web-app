import { locale } from 'shared/locale';

const initialState = { ...locale['ru-ru'] };

export default initialState;
