import { combineReducers } from 'redux';

import { reducer as authReducer } from 'features/user';
import { reducer as hostsReducer } from 'features/hosts';
import { reducer as reportsReducer } from 'features/reports';

export const reducer = combineReducers({
  user: authReducer,
  hosts: hostsReducer,
  reports: reportsReducer,
});
