import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { reducer } from './reducer';

export const configureStore = (extra) => {
  const middlewares = [thunk.withExtraArgument(extra)];

  const store = createStore(
    reducer,
    compose(
      applyMiddleware(...middlewares),
      window.__REDUX_DEVTOOLS_EXTENSION__
        ? window.__REDUX_DEVTOOLS_EXTENSION__({ trace: true })
        : (arg) => arg
    )
  );

  window._dispatch = store.dispatch;

  return store;
};
