import { useEffect, useCallback } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import { actions as authActions } from 'features/user';

export const useToken = () => {
  const dispatch = useDispatch();
  const getToken = useCallback(() => {
    dispatch(authActions.getToken());
  }, [dispatch]);

  useEffect(() => {
    getToken();
  }, [getToken]);

  const token = useSelector((state) => state.user.token, shallowEqual);

  return token;
};
