import { useMemo } from 'react';

export const useIsDataEmpty = (...items) =>
  useMemo(() => items?.every((item) => item.length === 0) || false, [items]);
