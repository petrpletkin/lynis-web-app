import React, { useState, useCallback, useContext } from 'react';

const initialAlertContextValue = {
  isOpen: false,
  title: '',
  description: '',
  successButtonText: '',
  successButtonColorScheme: 'blue',
  onSuccess: () => {},
};

export const AlertContext = React.createContext(initialAlertContextValue);

export const useAlertInitialValue = () => {
  const [alert, setAlert] = useState(initialAlertContextValue);

  const openAlert = useCallback(
    ({
      title = '',
      description = "Are you sure? You can't undo this action afterwards.",
      successButtonColorScheme = 'blue',
      successButtonText = '',
      onSuccess = () => {},
    }) => {
      setAlert({
        isOpen: true,
        title,
        description,
        onSuccess,
        successButtonText,
        successButtonColorScheme,
      });
    },
    []
  );
  const closeAlert = useCallback(
    () => setAlert(() => initialAlertContextValue),
    []
  );
  return {
    ...alert,
    openAlert,
    closeAlert,
  };
};

export const useAlertContext = () => useContext(AlertContext);
