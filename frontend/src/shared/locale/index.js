import localeRU from './ru-ru';
// import localeEU from './en-US';

export const languages = {
  RU: 'ru-ru',
  // EN: 'en-US',
};

export const pickLanguages = [
  { lang: languages.RU, text: 'Русский' },
  // { lang: languages.EN, text: 'English' },
];

export const shortLanguages = {
  'ru-ru': 'ru',
  // 'en-US': 'en',
};

export const locale = {
  'ru-ru': localeRU,
  // 'en-US': localeEU,
};
