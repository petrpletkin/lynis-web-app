import React from 'react';
import PropTypes from 'prop-types';

import { Spinner as ChakraSpinner, Center, Box } from '@chakra-ui/react';

const WithSpinner = ({ size = 'xl', isProcessing = false, children }) => (
  <Box pos="relative" display="flex" flexDirection="column" flexGrow="1">
    {isProcessing && (
      <Center bgColor="gray.500" opacity="0.7" pos="absolute" h="100%" w="100%">
        <ChakraSpinner
          thickness="4px"
          speed="0.65s"
          emptyColor="teal.100"
          color="teal.600"
          size={size}
        />
      </Center>
    )}
    {children}
  </Box>
);

WithSpinner.propTypes = {
  size: PropTypes.oneOf(['sm', 'md', 'lg', 'xl', 'xs']),
  isProcessing: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
};

export default React.memo(WithSpinner);
