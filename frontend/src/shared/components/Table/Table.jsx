import React, { useMemo } from 'react';
import PropTypes from 'prop-types';

import {
  Box,
  Heading,
  Table as CharkaTable,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from '@chakra-ui/react';

const Table = ({
  title,
  heads = [],
  rows = [],
  trows = [],
  isLast = false,
  containerProps = {},
  maxHeight = '350px',
}) => {
  const _heads = useMemo(
    () =>
      heads.map(({ value, props = {} }, index) => (
        <Th key={index} {...props}>
          {value}
        </Th>
      )),
    [heads]
  );

  const _trows = useMemo(
    () =>
      trows.length > 0
        ? trows
        : rows.map((row, rowIndex) => {
            if (typeof row !== 'object')
              return (
                <Tr key={rowIndex}>
                  <Td>{row}</Td>
                </Tr>
              );
            const cells = Object.values(row).map((cell, cellIndex) => (
              <Td key={cellIndex}>{cell}</Td>
            ));
            return <Tr key={rowIndex}>{cells}</Tr>;
          }),
    [rows, trows]
  );

  if (_trows.length === 0) return null;

  return (
    <Box
      border="1px"
      borderColor="gray.300"
      mb={isLast ? 0 : 5}
      {...containerProps}
    >
      {title && (
        <Heading as="h6" size="sm" variant="bgGray" p="3">
          {`${title} (${_trows.length})`}
        </Heading>
      )}
      <Box overflow="auto" maxH={maxHeight}>
        <CharkaTable colorScheme="gray" variant="striped">
          <Thead>
            <Tr>{_heads}</Tr>
          </Thead>
          <Tbody>{_trows}</Tbody>
        </CharkaTable>
      </Box>
    </Box>
  );
};

Table.propTypes = {
  containerProps: PropTypes.object,
  title: PropTypes.string,
  heads: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.element,
      ]).isRequired,
      props: PropTypes.object,
    })
  ).isRequired,
  rows: PropTypes.arrayOf(PropTypes.any),
  trows: PropTypes.arrayOf(PropTypes.element),
  isLast: PropTypes.bool,
};

export default React.memo(Table);
