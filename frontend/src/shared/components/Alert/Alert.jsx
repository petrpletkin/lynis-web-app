import React, { useCallback } from 'react';
import {
  AlertDialog,
  AlertDialogOverlay,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogBody,
  AlertDialogFooter,
  Button,
} from '@chakra-ui/react';

import { useAlertContext } from 'shared/context/AlertContext';

const Alert = () => {
  const cancelRef = React.useRef();
  const {
    isOpen,
    title = '',
    description = '',
    successButtonText = '',
    onSuccess = () => {},
    successButtonColorScheme,
    closeAlert,
  } = useAlertContext();
  const onSuccessClick = useCallback(() => {
    onSuccess();
    closeAlert();
  }, [closeAlert, onSuccess]);
  return (
    <AlertDialog
      isOpen={isOpen}
      leastDestructiveRef={cancelRef}
      onClose={closeAlert}
    >
      <AlertDialogOverlay>
        <AlertDialogContent>
          <AlertDialogHeader fontSize="lg" fontWeight="bold">
            {title}
          </AlertDialogHeader>

          <AlertDialogBody>{description}</AlertDialogBody>

          <AlertDialogFooter>
            <Button ref={cancelRef} onClick={closeAlert}>
              Cancel
            </Button>
            <Button
              colorScheme={successButtonColorScheme}
              onClick={onSuccessClick}
              ml={3}
            >
              {successButtonText}
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialogOverlay>
    </AlertDialog>
  );
};

export default React.memo(Alert);
