import React from 'react';
import PropTypes from 'prop-types';

import { Box, Heading, Text, Wrap, WrapItem } from '@chakra-ui/react';

const InfoBlock = ({ title, items = [], wItem = '32%', isLast = false }) => {
  if (items.length === 0) return null;
  return (
    <Box border="1px" borderColor="gray.300" mb={isLast ? 0 : 5}>
      <Heading as="h6" size="sm" variant="bgGray" p="3">
        {title}
      </Heading>
      <Wrap p="3">
        {items.map(({ label, value, wItem: _wItem }) => (
          <WrapItem
            key={label}
            w={_wItem || wItem}
            display="flex"
            alignItems="center"
          >
            <Text fontWeight="bold" mr="1">
              {`${label}:`}
            </Text>
            {value}
          </WrapItem>
        ))}
      </Wrap>
    </Box>
  );
};

InfoBlock.propTypes = {
  title: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.element,
      ]).isRequired,
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.element,
      ]).isRequired,
    })
  ),
  wItem: PropTypes.string,
  isLast: PropTypes.bool,
};

export default React.memo(InfoBlock);
