import React, { useMemo } from 'react';

import { MoonIcon, SunIcon } from '@chakra-ui/icons';

import { IconButton, useColorMode } from '@chakra-ui/react';

const ColorModeButton = () => {
  const { colorMode, toggleColorMode } = useColorMode();

  const colorModeIcon = useMemo(
    () =>
      colorMode === 'light' ? (
        <MoonIcon color="blue.700" />
      ) : (
        <SunIcon color="yellow.400" />
      ),
    [colorMode]
  );
  return (
    <IconButton
      icon={colorModeIcon}
      onClick={toggleColorMode}
      bgColor="transparent"
    />
  );
};

export default React.memo(ColorModeButton);
