import React, { useCallback, useEffect, useMemo } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import { actions as userActions } from 'features/user';

import { RiLogoutCircleRLine } from 'react-icons/ri';

import {
  Box,
  Link as ChakraLink,
  Avatar,
  Text,
  Divider,
  IconButton,
  Icon,
} from '@chakra-ui/react';

import ColorModeButton from 'shared/components/ColorModeButton';

import { menuItems } from './data/menuItems';

const SideMenu = () => {
  const sideMenuItem = useMemo(
    () =>
      menuItems.map(({ link, title }) => (
        <ChakraLink
          key={link}
          as={Link}
          to={link}
          w="100%"
          display="flex"
          alignItems="center"
          minH="35px"
          p="2"
        >
          {title}
        </ChakraLink>
      )),
    []
  );

  const username = useSelector((state) => state.user.username, shallowEqual);

  const dispatch = useDispatch();
  const logout = useCallback(() => {
    dispatch(userActions.logout());
  }, [dispatch]);

  const token = useSelector((state) => state.user.token, shallowEqual);

  useEffect(() => {
    if (token) {
      dispatch(userActions.getUserInfo());
    }
  }, [dispatch, token]);

  return (
    <>
      <Box w="170px" flexShrink="0" p="2" display="flex" flexDirection="column">
        <Box p="2" pb="3">
          <Avatar bg="teal.500" />
          <Text>{username}</Text>
        </Box>
        <Divider orientation="horizontal" />
        <Box pt="3" pb="3">
          {sideMenuItem}
        </Box>
        <Box mt="auto" display="flex">
          <Box mr="2">
            <ColorModeButton />
          </Box>
          <IconButton
            aria-label="Logout"
            onClick={logout}
            colorScheme="gray"
            icon={<Icon as={RiLogoutCircleRLine} />}
            ml="auto"
          />
        </Box>
      </Box>

      <Divider orientation="vertical" />
    </>
  );
};

export default React.memo(SideMenu);
