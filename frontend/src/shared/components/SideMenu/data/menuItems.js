export const menuItems = [
  {
    link: '/hosts',
    title: 'Hosts',
  },
  {
    link: '/reports',
    title: 'Reports',
  },
];
