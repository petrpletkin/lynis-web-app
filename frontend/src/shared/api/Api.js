import UserApi from './UserApi';
import HostsApi from './HostsApi';
import ReportsApi from './ReportsApi';

class Api {
  constructor(baseUrl = '') {
    this.user = new UserApi(baseUrl);
    this.hosts = new HostsApi(baseUrl);
    this.reports = new ReportsApi(baseUrl);
  }
}

export default Api;
