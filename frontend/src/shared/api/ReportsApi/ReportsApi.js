import { BaseApi } from '../BaseApi';

import { ReportsConverter } from './ReportsConverter';

class ReportsApi extends BaseApi {
  constructor(baseUrl) {
    super();
    this.baseUrl = `${baseUrl}/reports`;
    this.converter = new ReportsConverter();
  }

  getReports = () =>
    this.sendQuery({
      type: this.queryTypes.GET,
      url: `${this.baseUrl}`,
      converter: this.converter.convertReports,
    });

  getReport = (id) =>
    this.sendQuery({
      type: this.queryTypes.GET,
      url: `${this.baseUrl}/${id}`,
      converter: this.converter.convertReport,
    });

  createReport = (reports) =>
    this.sendQuery({
      type: this.queryTypes.PUT,
      url: this.baseUrl,
      requestData: reports,
    });
}

export default ReportsApi;
