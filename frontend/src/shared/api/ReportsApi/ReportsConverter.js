import dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'shared/constants';

import { convertToArray, convertToBool } from 'shared/utils/converter';

export class ReportsConverter {
  convertReports = (data) => {
    const reports =
      data
        ?.map(({ _id, host, created_at }) => ({
          id: _id,
          host,
          createdDate: created_at
            ? dayjs(created_at).format(DATE_TIME_FORMAT)
            : '-',
          timestamp: +dayjs(created_at || new Date()),
        }))
        ?.sort((a, b) => b.timestamp - a.timestamp)
        ?.filter(({ id, host, createdDate }) => ({
          id,
          host,
          createdDate,
        })) || [];
    return reports;
  };

  convertReport = ({ _id, host, created_at, report }) => ({
    id: _id,
    host: host,
    createdDate: dayjs(created_at).format(DATE_TIME_FORMAT),
    hostFinding: {
      auditor: report?.host_finding?.auditor || '-',
      hardeningIndex: +(report?.host_finding?.hardening_index || -1),
    },
    warnings: convertToArray(report?.['warning[]']).map((warning) => {
      const [test = '', description = '', details = '', solution = ''] =
        warning.split('|');
      return {
        description,
        test,
        details,
        solution,
      };
    }),
    suggestions: convertToArray(report?.['suggestion[]']).map((suggestion) => ({
      id: suggestion.id,
      description: suggestion.description,
      severity: suggestion?.severity || '-',
    })),
    manuals: convertToArray(report?.['manual[]']),
    vulnerablePackages: convertToArray(report?.['vulnerable_packag[]']),
    lynisInfo: {
      lynisVersion: report?.lynis_info?.lynis_version || '-',
      lynisTestsDone: convertToBool(report?.lynis_info?.lynis_tests_done),
      lynisUpdateAvailable: convertToBool(
        report?.lynis_info?.lynis_update_available
      ),
      licenseKey: report?.lynis_info?.license_key || '-',
      reportDatetimeStart: report?.lynis_info?.report_datetime_start
        ? dayjs(report.lynis_info.report_datetime_start).format(
            DATE_TIME_FORMAT
          )
        : '-',
      reportDatetimeEnd: report?.lynis_info?.report_datetime_end
        ? dayjs(report.lynis_info.report_datetime_end).format(DATE_TIME_FORMAT)
        : '-',
      reportVersionMajor: report?.lynis_info?.report_version_major || '-',
      reportVersionMinor: report?.lynis_info?.report_version_minor || '-',
      pluginEnabledPhases: convertToArray(
        report?.lynis_info?.['plugin_enabled_phase[]']
      ).map((item) => ({
        name: item?.name || '-',
        version: item?.version || '-',
      })),
    },
    hostInfo: {
      hostname: report?.host_info?.hostname || '-',
      domainName: report?.host_info?.domainname || '-',
      // TODO: check value
      resolveConfSearchDomains: convertToArray(
        report?.host_info?.['resolv_conf_search_domain[]']
      ),
      osFullname: report?.host_info?.os_fullname || '-',
      osKernelVersionFull: report?.host_info?.os_kernel_version_full || '-',
      packageAuditTool: report?.host_info?.package_audit_tool || '-',
      packageAuditToolFound: report?.host_info?.package_audit_tool_found || '-',
      memorySize: report?.host_info?.memory_size || '-',
      selinux: {
        status: convertToBool(report?.host_info?.selinux?.status),
        mode: report?.host_info?.selinux?.mode || '-',
      },
      cpuPae: report?.host_info?.cpu_pae || '-',
      cpuNx: report?.host_info?.cpu_nx || '-',
      uptimeInDays: report?.host_info?.uptime_in_days || '-',
      uptimeInSeconds: report?.host_info?.uptime_in_seconds || '-',
      locateDb: report?.host_info?.locate_db || '-',
      availableShells: convertToArray(report?.host_info?.['available_shell[]']),
      isNotebook: convertToBool(report?.host_info?.is_notebook),
      isContainer: convertToBool(report?.host_info?.is_container),
      binaryPaths: convertToArray(report?.host_info?.binary_paths),
      cronJobs: convertToArray(report?.host_info?.['cron_job[]']),
      loggingInfo: {
        logRotationConfigFound: convertToBool(
          report?.host_info?.logging_info?.log_rotation_config_found
        ),
        logRotationTool:
          report?.host_info?.logging_info?.log_rotation_tool || '-',
        syslogDaemons: convertToArray(
          report?.host_info?.logging_info?.['syslog_daemon[]']
        ),
        logDirectories: convertToArray(
          report?.host_info?.logging_info?.['log_directory[]']
        ),
      },
    },
    networkInfo: {
      ipv6Mode: report?.network_info?.ipv6_mode || '-',
      ipv6Only: convertToBool(report?.network_info?.ipv6_only),
      networkInterfaces: convertToArray(
        report?.network_info?.['network_interface[]']
      ),
      networkIpv4Addresses: convertToArray(
        report?.network_info?.['network_ipv4_address[]']
      ),
      networkIpv6Addresses: convertToArray(
        report?.network_info?.['network_ipv6_address[]']
      ),
      defaultGateway: report?.network_info?.default_gateway || '-',
      networkMacAddresses: convertToArray(
        report?.network_info?.['network_mac_address[]']
      ),
      nameCacheUsed: convertToBool(report?.network_info?.name_cache_used),
      nameServers: convertToArray(report?.network_info?.nameservers),
      resolveConfSearchDomains: convertToArray(
        report?.network_info?.resolv_conf_search_domain
      ),
      networkListenPorts: convertToArray(
        report?.network_info?.['network_listen_port[]']
      ).map((item) => ({
        port: item?.port || '',
        ownerProcess: item?.owner_process || '',
        protocol: item?.protocol || '',
      })),
    },
    securityInfo: {
      firewallInstalled: convertToBool(
        report?.security_info?.firewall_installed
      ),
      firewallSoftwares: convertToArray(
        report?.security_info?.['firewall_software[]']
      ),
      firewallEmptyRuleset: convertToBool(
        report?.security_info?.firewall_empty_ruleset
      ),
      packageAuditTool: report?.security_info?.package_audit_tool || '-',
      packageAuditToolFound: convertToBool(
        report?.security_info?.package_audit_tool_found
      ),
      packageManagers: convertToArray(
        report?.security_info?.['package_manager[]']
      ),
      authenticationTwoFactorEnabled: convertToBool(
        report?.security_info?.authentication_two_factor_enabled
      ),
      authenticationTwoFactorRequired: convertToBool(
        report?.security_info?.authentication_two_factor_required
      ),
      ldapAuthEnabled: convertToBool(report?.security_info?.ldap_auth_enabled),
      ldapPamEnabled: convertToBool(report?.security_info?.ldap_pam_enabled),
      passwordMaxDays: report?.security_info?.password_max_days || '-',
      passwordMinDays: report?.security_info?.password_min_days || '-',
      automationToolPresent: convertToBool(
        report?.security_info?.automation_tool_present
      ),
      automationToolsRunning: convertToArray(
        report?.security_info?.['automation_tool_running[]']
      ),
      pamPwquality: report?.security_info?.pam_pwquality || '-',
      compilers: convertToArray(report?.security_info?.['compiler[]']),
      compilerInstalled: convertToBool(
        report?.security_info?.compiler_installed
      ),
      idsIpsTooling: convertToArray(
        report?.security_info?.['ids_ips_tooling[]']
      ),
      authFailedLoginsLogged: convertToBool(
        report?.security_info?.auth_failed_logins_logged
      ),
      fail2banConfigs: convertToArray(report?.security_info?.fail2ban_config),
      fail2banEnabledServices: convertToArray(
        report?.security_info?.['fail2ban_enabled_service[]']
      ),
      apparmorEnabled: convertToBool(report?.security_info?.apparmor_enabled),
      apparmorPolicyLoaded: convertToBool(
        report?.security_info?.apparmor_policy_loaded
      ),
      authGroupIdsUnique: convertToBool(
        report?.security_info?.auth_group_ids_unique
      ),
      authGroupNamesUnique: convertToBool(
        report?.security_info?.auth_group_names_unique
      ),
      realUsers: convertToArray(report?.security_info?.['real_user[]']).map(
        ([name, uid]) => ({
          name,
          uid,
        })
      ),
      homeDirectories: convertToArray(
        report?.security_info?.['home_directory[]']
      ),
    },
    bootInfo: {
      bootServiceTool: report?.boot_info?.boot_service_tool || '-',
      bootUefiBooted: convertToBool(report?.boot_info?.boot_uefi_booted),
      bootUefiBootedSecure: convertToBool(
        report?.boot_info?.boot_uefi_booted_secure
      ),
      linuxDefaultRunlevel: report?.boot_info?.linux_default_runlevel || '-',
      servicesStartedAtBoot: convertToArray(
        report?.boot_info?.['services_started_at_boot[]']
      ),
      bootLoader: report?.boot_info?.boot_loader || '-',
    },
  });
}
