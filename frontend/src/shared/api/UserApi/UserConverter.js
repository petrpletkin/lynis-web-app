export class UserConverter {
  convertToken = (data) => data.access_token;

  convertInfo = (data) => ({
    id: data._id,
    username: data.username,
    email: data.email,
    hosts: data.hosts,
  });
}
