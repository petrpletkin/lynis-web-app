import { BaseApi } from '../BaseApi';
import { UserConverter } from './UserConverter';

class UserApi extends BaseApi {
  constructor(baseUrl) {
    super();
    this.baseUrl = `${baseUrl}/users`;
    this.converter = new UserConverter();
  }

  login = ({ username, password }) =>
    this.sendQuery({
      type: this.queryTypes.POST,
      url: `${this.baseUrl}/auth`,
      requestData: { username, password },
      options: {},
      converter: this.converter.convertToken,
    });

  getInfo = () =>
    this.sendQuery({
      type: this.queryTypes.GET,
      url: `${this.baseUrl}/info`,
      options: {},
      converter: this.converter.convertInfo,
    });
}

export default UserApi;
