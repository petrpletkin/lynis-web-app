import { actions as userActions } from 'features/user';
import { AccessTokenManager } from 'shared/managers/AccessTokenManager';

import { notify } from 'features/notification';

import HttpActions from './HttpActions';
import { QueryTypes } from './QueryTypes';

export class BaseApi {
  baseUrl = '';

  static lang = '';

  constructor(baseUrl = '', version = '') {
    this.baseUrl = baseUrl;
    this.actions = new HttpActions(baseUrl, version);
    this.queryTypes = QueryTypes;
  }

  sendQuery = async ({
    type,
    url,
    requestData = {},
    options = {},
    converter = (dt) => dt,
  }) => {
    const _options = { ...options };
    const token = AccessTokenManager.getToken();
    if (token) _options.headers = { Authorization: `Bearer ${token}` };

    let response;
    switch (type) {
      case QueryTypes.POST:
        response = await this.actions.post(url, requestData, _options);
        break;
      case QueryTypes.DELETE:
        response = await this.actions.delete(url, requestData, _options);
        break;
      case QueryTypes.PUT:
        response = await this.actions.put(url, requestData, _options);
        break;
      default:
        response = await this.actions.get(url, null, _options);
        break;
    }

    const { data, status } = response;
    const success = status === 200 || data === null;

    if (status === 401) window._dispatch(userActions.logout());

    if (!success) {
      notify({
        title: 'Network error',
        description: data?.detail || 'Request execution error',
        status: 'error',
      });
    }

    const resultResponse = {
      success,
      data: success && converter(data),
      errorMessage: data?.detail || 'Request execution error',
      codeStatus: data?.status || status,
    };
    return resultResponse;
  };
}
