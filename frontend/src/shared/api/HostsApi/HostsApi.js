import { BaseApi } from '../BaseApi';
import { HostsConverter } from './HostsConveter';

class HostsApi extends BaseApi {
  constructor(baseUrl) {
    super();
    this.baseUrl = `${baseUrl}/hosts`;
    this.converter = new HostsConverter();
  }

  getHosts = () =>
    this.sendQuery({
      type: this.queryTypes.GET,
      url: `${this.baseUrl}`,
      converter: this.converter.convertHosts,
    });

  getHost = (id) =>
    this.sendQuery({
      type: this.queryTypes.GET,
      url: `${this.baseUrl}/${id}`,
      converter: this.converter.convertHost,
    });

  createHost = ({ host, port, username, alias }) =>
    this.sendQuery({
      type: this.queryTypes.POST,
      url: `${this.baseUrl}`,
      requestData: {
        host,
        port,
        username,
        alias,
      },
    });

  updateHost = ({ id, port, host, username, alias }) =>
    this.sendQuery({
      type: this.queryTypes.PUT,
      url: `${this.baseUrl}/${id}`,
      requestData: {
        port,
        host,
        username,
        alias,
      },
    });

  deleteHost = (id) =>
    this.sendQuery({
      type: this.queryTypes.DELETE,
      url: `${this.baseUrl}/${id}`,
    });
}

export default HostsApi;
