import dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'shared/constants';

export class HostsConverter {
  convertHosts = (data) =>
    data?.map((item) => ({
      id: item._id,
      host: item.host,
      username: item.username,
      port: item.port,
      createdDate: dayjs(item.created_at).format(DATE_TIME_FORMAT),
      lastUpdateDate: item.last_update_timestamp
        ? dayjs(item.last_update_timestamp).format(DATE_TIME_FORMAT)
        : '-',
      suggestions: item.extra_data?.['suggestion[]'] ?? '-',
      isRunningNow: item?.is_running_now || false,
      warnings: item.extra_data?.['warning[]'] ?? '-',
      osFullname: item.extra_data?.os_fullname || '-',
      hardeningIndex: +(item.extra_data?.hardening_index || -1),
      alias: item.alias,
      reports:
        item?.reports?.map((report) => ({
          id: report._id,
          createdDate: dayjs(report.created_at).format(DATE_TIME_FORMAT),
        })) || [],
    })) || [];

  convertHost = (data) => {
    if (!data) return null;
    return {
      id: data._id,
      host: data.host,
      username: data.username,
      port: data.port,
      alias: data.alias,
      createdDate: dayjs(data.created_at).format(DATE_TIME_FORMAT),
      lastUpdateDate: data.last_update_timestamp
        ? dayjs(data.last_update_timestamp).format(DATE_TIME_FORMAT)
        : '-',
      isRunningNow: data?.is_running_now || false,
      osFullname: data.extra_data?.os_fullname || '',
      suggestions:
        data.extra_data?.['suggestion[]']?.map((suggestion) => ({
          id: suggestion.id,
          description: suggestion.description,
          severity: suggestion?.severity || '-',
        })) || [],
      warnings:
        data.extra_data?.['warning[]'].map((warning) => {
          const [test = '', description = ''] = warning.split('|');
          return {
            description,
            test,
          };
        }) || [],
      hardeningIndex: +(data.extra_data?.hardening_index || -1),
      reports:
        data.extra_data?.reports.map((report) => ({
          id: report._id,
          createdDate: dayjs(report.created_at).format(DATE_TIME_FORMAT),
          shortDate: dayjs(report.created_at).format('DD.MM hh:mm'),
          suggestions: report.suggestions_count,
          warnings: report.warnings_count,
        })) || [],
    };
  };
}
