export const convertToArray = (arr) => {
  if (!arr || arr === 'NA') return [];
  if (typeof arr === 'string') return [arr];
  return arr;
};

export const convertToBool = (value, defaultValue = false) =>
  !!+(value || defaultValue ? '1' : '0');
