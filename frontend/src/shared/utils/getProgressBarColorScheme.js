export const getProgressBarColorScheme = (value) => {
  if (value === -1) return 'gray';
  if (value < 35) return 'red';
  if (value < 60) return 'yellow';
  return 'green';
};
