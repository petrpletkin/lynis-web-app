import { extendTheme } from '@chakra-ui/react';

const config = {
  initialColorMode: 'light',
  useSystemColorMode: false,
};

export const theme = extendTheme({
  config,
  components: {
    Heading: {
      variants: {
        bgGray: (props) => ({
          bgColor: props.colorMode === 'dark' ? 'gray.700' : 'gray.100',
        }),
      },
    },
  },
});
