const ACCESS_TOKEN_KEY = 'accessToken';

export const AccessTokenManager = {
  getToken: () => {
    const token = localStorage.getItem(ACCESS_TOKEN_KEY);
    if (token === 'null') return null;
    return token;
  },
  setToken: (value) => localStorage.setItem(ACCESS_TOKEN_KEY, value),
};
