import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import { ChakraProvider } from '@chakra-ui/react';

import 'normalize.css';
import 'shared/style/bootstrap.scss';

import { configureStore } from './store/configureStore';

import App from './modules/App';

import { modules } from './modules';

import Api from './shared/api';
import { apiBaseUrl } from './shared/api/baseUrl';
import { createRoutes } from './routes/createRoutes';

const api = new Api(apiBaseUrl);

const childrens = createRoutes(modules);

const store = configureStore({ api });

ReactDOM.render(
  <React.StrictMode>
    <ChakraProvider>
      <Provider store={store}>
        <BrowserRouter>
          <App>{childrens}</App>
        </BrowserRouter>
      </Provider>
    </ChakraProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
